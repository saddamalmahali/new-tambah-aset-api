//Authentication Routes
//By Fikri akhdi
var jwt = require('../middleware/jwt');
module.exports = function (app) {
  var authentication = require('../controllers/authentication');
  // Routes
  app.route('/auth/login')
    .post(authentication.token)
  app.route('/auth/refresh_token')
    .get(jwt.validate(),authentication.refresh_token)
  app.route('/auth/logout')
    .post(authentication.logout)
}