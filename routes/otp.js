//OTP Routes
//By Fikri akhdi
var jwt = require('../middleware/jwt');
module.exports = function (app) {
  var otp = require('../controllers/otp');
  // Routes
  app.route('/otp/request')
    .post(jwt.validate(),otp.request)
  app.route('/otp/check/:code')
    .get(jwt.validate(),otp.check)
}