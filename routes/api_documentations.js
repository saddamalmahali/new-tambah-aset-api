//Api Documentations Routes
//By Fikri akhdi
var jwt = require('../middleware/jwt');
module.exports = function (app) {
    var api_documentations = require('../controllers/api_documentations');
    // Routes
    app.route('/api_documentations/list')
      .get(jwt.validate(),api_documentations.list)
}