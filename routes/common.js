//Common Routes
//By Fikri akhdi
module.exports = function (app) {
  var common = require('../controllers/common');
  // Routes
  app.route('/common/banks')
    .get(common.banks)
  app.route('/common/roles')
    .get(common.roles)
  app.route('/common/verify/:id')
    .get(common.verify)
  app.route('/common/reset_password')
    .post(common.reset_password)
  app.route('/common/provinces')
    .get(common.provinces)
  app.route('/common/cities/:id')
    .get(common.cities)
  app.route('/common/districts/:id')
    .get(common.districts)
  app.route('/common/subdistricts/:id')
    .get(common.subdistricts)
  app.route('/common/business_sections')
    .get(common.business_sections)
    
}