
var apiHelper = require('../helpers/api')
const jwt = require('jsonwebtoken')
const fs = require('fs');
var publicKEY = fs.readFileSync('./key/public.key', 'utf8')
exports.validate = function (options) {
    return function (req, res, next) {
        res.type('json')
        if (req.header('authorization')) {
            try {
            var token = req.header('authorization').replace('Bearer ', ''),
                decoded
            var dateNow = new Date()
                decoded = jwt.verify(token, publicKEY)
                console.log(decoded.exp, dateNow.getTime() / 1000)
                if (decoded.exp < dateNow.getTime() / 1000) {
                    res.end(apiHelper.api_format(false, "Your token is expired", 400))
                } else {
                    res.user_id = decoded.id
                    console.log("User ID "+decoded.id)
                    next()
                }
            } catch (e) {
                console.log(e)
                if (e.name === 'UnauthorizedError' || e.name === 'JsonWebTokenError' || e.name === 'TokenExpiredError') {
                    res.status(401).send(apiHelper.api_format(false, e.name, 401));
                    return;
                } else
                    res.status(400).end(apiHelper.api_format(false, "Something went wrong", 400))
            }
            } else {
                res.end(apiHelper.api_format(false, "Token Required", 400))
            }
    }
  }
