
var apiHelper = require('../helpers/api')
const jwt = require('jsonwebtoken')
const usersRepo = require('../repositories/users')
const whitelistsRepo = require('../repositories/whitelists')
const fs = require('fs');
var publicKEY = fs.readFileSync('./key/client_public.key', 'utf8')
exports.validate =  function (options) {
    return  function (req, res, next) {
        res.type('json')
        if (req.header('authorization')) {
            var ipAddress = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            (req.connection.socket ? req.connection.socket.remoteAddress : null)
            var ipAddr = ipAddress.split(",")[0]
            try {
        var token = req.header('authorization').replace('Bearer ', ''),
            decoded
        var dateNow = new Date()
            decoded = jwt.verify(token, publicKEY)
            console.log(decoded.exp, dateNow.getTime() / 1000)
            if (decoded.exp < dateNow.getTime() / 1000) {
                res.end(apiHelper.api_format(false, "Your token is expired", 400))
            } else {
            var where = {
                api_token: token
            }
            usersRepo.findById(decoded.client_id).then(user=>{
                if(user){
                    where = {
                        company_id: user.company_id,
                        ip_address: ipAddr,
                        is_deleted: null,
                    }
                    whitelistsRepo.findByWhere(where).then((whitelist_check)=>{
                        if(whitelist_check.length > 0){
                            //Check Whitelist
                            res.user_id = user.id
                            console.log("User ID "+user.id)
                            next()
                        } else {
                            res.status(400).end(apiHelper.api_format(false, `Your IP ${ipAddr} is not in the whitelist `, 400))
                        }
                    })
                    
                } else {
                    res.status(400).end(apiHelper.api_format(false, "Invalid API Key", 400))
                
            }
            })
        }
            }catch (e) {
                console.log(e)
                if (e.name === 'UnauthorizedError' || e.name === 'JsonWebTokenError' || e.name === 'TokenExpiredError') {
                    res.status(401).send(apiHelper.api_format(false, e.name, 401));
                    return;
                } else
                    res.status(400).end(apiHelper.api_format(false, "Something went wrong", 400))
            }
            } else {
                res.end(apiHelper.api_format(false, "Token Required", 400))
            }
    }
  }
