var Imap = require("imap");
var simpleParser = require("mailparser").simpleParser
const userMailsAttachmentRepo = require('../repositories/user_mails_attachments')
const userMailsRepo = require('../repositories/user_mails')
const usersRepo = require('../repositories/users')
const userMailsPersonsRepo = require('../repositories/user_mails_persons')
var commonHelper = require('../helpers/common')
var crypto = require('crypto')
var Promise = require("bluebird")
Promise.longStackTraces()


async function setImap(user, password, host, port,tls){
    var imapConfig = {
        user: user, 
        password: password, 
        host: host, 
        port: port,
        tls: tls,
        secure: false
      };
      console.log("Imap Config", imapConfig)
 var imap = new Imap(imapConfig);

 Promise.promisifyAll(imap)
 return imap
} 
async function FetchMailsFirst(userId,imap, syncType, box) {
            imap.openBox(box.toString(), false, function(err, mailBox) {
                if (err) {
                    console.error(err);
                    return;
                }
                var filter = ["ALL"]
                var now = new Date()
                var minus = 1
                const monthNames = ["","January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];
        
                if(syncType=='one_month') minus=1
                if(syncType=='two_month') minus=2
                if(syncType=='three_month') minus=4
                if(syncType=='four_month') minus=4

                console.log("MINS MONTH : ", minus)
                var MonthBefore = now
                MonthBefore.setMonth(MonthBefore.getMonth() - minus)
                MonthBefore.setHours(0, 0, 0);
                MonthBefore.setMilliseconds(0)
                var year_before = MonthBefore.getFullYear()
                var month_before = MonthBefore.getMonth()
                var date_before = MonthBefore.getDate()
                var year_after = now.getFullYear() 
                var month_after = now.getMonth()+1
                var date_after = now.getDate()
                console.log(month_before)
                console.log("Month : ",monthNames[month_after])
                if(syncType!="now") {
                    filter.push( ['SINCE', monthNames[month_before]+' '+date_before+', '+year_before], ['BEFORE', monthNames[month_after]+' '+date_after+', '+year_after])
                    // filter.push( ['SINCE', monthNames[month_after]+' '+date_after+', '+year_after], ['BEFORE', monthNames[month_before]+' '+date_before+', '+year_before])
                    // filter.push( ['SINCE', monthNames[month_after]+' '+date_after+', '+year_after])
                }
                console.log("FILTER : ", filter)
                imap.search(filter, function(err, results) {
                      console.log("results")
                      console.log(results)
                    // if(!results || !results.length){console.log("No unread mails");imap.end();return;}
                    /* mark as seen
                    imap.setFlags(results, ['\\Seen'], function(err) {
                        if (!err) {
                            console.log("marked as read");
                        } else {
                            console.log(JSON.stringify(err, null, 2));
                        }
                    });*/
        
                    var f = imap.fetch(results, { bodies:  ['HEADER.FIELDS (TO FROM SUBJECT DATE CC BCC)',''], markSeen: true });
                    f.on("message", async function(msg, seqno) {
                        console.log("Processing msg #" + seqno);
                        msg.on('attributes', function (attrs) {
                            // partID = findTextPart(attrs.struct);
                            // const attachments = findAttachmentParts(attrs.struct);
                            // attachments.forEach((attachment) => {
                            //   const filename = attachment.params.name  // need decode disposition.params['filename*'] !!!
                            //   const encoding = attachment.encoding.toString().toUpperCase()
                            //   const f = imap.fetch(attrs.uid, { bodies: [attachment.partID] })
                            // })
                          });
                        msg.on("body", function(stream) {
                            simpleParser(stream, async(err, mail) => {
                                // console.log("MAIL ", mail)
                                var subject_hash= crypto.createHash("sha256")
                                .update(mail.subject)
                                .digest("hex")
                                // console.log(mail)
                                var from        = mail.from.value[0]
                                var subject     = mail.subject
                                var html        = mail.html
                                var text        = mail.text
                                // console.log("TEXT ", text)
                                var messageId   = (mail.messageId?mail.messageId:subject_hash)
                                var date        = mail.date
                                // console.log("Sender Mail ", from)
                                if(from && html)
                                await saveMessage(userId,from, subject, (text?text.substr(0,200):html.replace(/<[^>]*>?/gm, '').substr(0,200)), html,box.toString().toUpperCase(),messageId, date)
                              });
                            // stream.on("data", function(chunk) {
                            //     // parser.write(chunk.toString("utf8"));
                            // });
                        });
                        msg.once("end", function() {
                            console.log("Finished msg #" + seqno);
                            // parser.end();
                        });
                    }
                    );
                    f.once("error", function(err) {
                        return Promise.reject(err);
                    });
                    f.once("end", function() {
                        console.log("Done fetching all unseen messages.");
                        imap.end();
                    });
                });
            });
}

async function FetchMails(userId,imap, syncType, box) {
    imap.openBox(box, false, function(err, mailBox) {
        if (err) {
            console.error(err);
            return;
        }
        var filter = ["ALL"]
        var now = new Date()
        var minus = 1
        const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

        if(syncType=='one_month') minus=1
        if(syncType=='two_month') minus=2
        if(syncType=='three_month') minus=4
        if(syncType=='four_month') minus=4

        var MonthBefore = now
        MonthBefore.setMonth(MonthBefore.getMonth() - minus)
        MonthBefore.setHours(0, 0, 0);
        MonthBefore.setMilliseconds(0)
        var year_before = MonthBefore.getFullYear()
        var month_before = MonthBefore.getMonth()+1
        var date_before = MonthBefore.getDate()
        var year_after = now.getFullYear() 
        var month_after = now.getMonth()+1
        var date_after = now.getDate()
        console.log("Months Before ",month_before)
        console.log(monthNames[month_before])
        if(syncType!="now") {
            // filter.push( ['SINCE', monthNames[month_after]+' '+date_after+', '+year_after], ['BEFORE', monthNames[month_before]+' '+date_before+', '+year_before])
            filter.push( ['SINCE', monthNames[month_after]+' '+date_after+', '+year_after])
        }
        imap.search(["ALL"], function(err, results) {
            if(!results || !results.length){console.log("No unread mails");imap.end();return;}
            /* mark as seen
            imap.setFlags(results, ['\\Seen'], function(err) {
                if (!err) {
                    console.log("marked as read");
                } else {
                    console.log(JSON.stringify(err, null, 2));
                }
            });*/

            var f = imap.fetch(results, { bodies:  ['HEADER.FIELDS (TO FROM SUBJECT DATE CC BCC)',''], markSeen: true });
            f.on("message", async function(msg, seqno) {
                console.log("Processing msg #" + seqno);
                msg.on('attributes', function (attrs) {
                    // partID = findTextPart(attrs.struct);
                    // const attachments = findAttachmentParts(attrs.struct);
                    // attachments.forEach((attachment) => {
                    //   const filename = attachment.params.name  // need decode disposition.params['filename*'] !!!
                    //   const encoding = attachment.encoding.toString().toUpperCase()
                    //   const f = imap.fetch(attrs.uid, { bodies: [attachment.partID] })
                    // })
                  });
                msg.on("body", function(stream) {
                    simpleParser(stream, async(err, mail) => {
                        var from        = mail.from.value
                        var subject     = mail.subject
                        var html        = mail.html
                        var text        = mail.text
                        var messageId   = mail.messageId
                        var date        = mail.date
                        await saveMessage(userId,from, subject, text, html,box.toString().toUpperCase(),messageId, date)
                      });
                    // stream.on("data", function(chunk) {
                    //     // parser.write(chunk.toString("utf8"));
                    // });
                });
                msg.once("end", function() {
                    console.log("Finished msg #" + seqno);
                    // parser.end();
                });
            }
            );
            f.once("error", function(err) {
                return Promise.reject(err);
            });
            f.once("end", function() {
                console.log("Done fetching all unseen messages.");
                imap.end();
            });
        });
    });
}




async function saveMessage (userId,from,subject, snippet, html,labelIds, messageId, date) {
    new Promise(async (resolve, reject)=>{
        if(typeof messageId=="undefined") messageId = crypto.createHash('md5').update(subject).digest('hex')
        var where = {
          user_mail_mailId : messageId
        }
        var found = await userMailsRepo.findByWhere(where)
        if(found){
          console.log("Not Found, Saving Message")
          var where   = {
            user_mails_person_user_id : userId
          }
          if(from.address){
            where.user_mails_person_email = from.address
          }
          var person = await userMailsPersonsRepo.findOneByWhere(where)
            //insert
            var dataCreate = {
                user_mail_user_id : userId,
                user_mail_sender_email : from.address,
                user_mail_sender_name : from.name,
                user_mail_content : snippet,
                user_mail_receiver_name : null,
                user_mail_receiver_email :null,
                user_mail_snippet : snippet,
                user_mail_historyId : messageId, 
                user_mail_date : date,
                user_mail_body: html,
                user_mail_labelIds: labelIds.toString(),
                user_mail_subject: removeEmojis(subject.toString()),
                user_mail_mailId: messageId,
                user_mail_threadId: messageId,
                user_mail_created_time: date,
                user_mail_person_id: (person?person.user_mails_person_person_id:null)
            }
            console.log("Mail Data : ", dataCreate)
            var res = await userMailsRepo.create(dataCreate)
            var mailId = res.user_mail_id
            console.log("MAIL ID", mailId)
            var mail    = await userMailsRepo.findById(mailId)
            console.log("ISI MAIL", mail)
            if(mail ){
                var where       = {
                    user_mails_attachment_mail_id : mail.user_mail_id
                }
                var attachments = await userMailsAttachmentRepo.findByWhere(where)
                mail.setDataValue('attachments', attachments)
            if(mail) {
              resolve(mail)
            //   console.log("Emitting Socket", mail)
              await commonHelper.create_notification(userId,'NEW_MAIL', mail.user_mail_id, date)
              var user = await usersRepo.findById(userId)
              var notification = {
                  "total_count": user.user_notification_total_count,
                  "activity_count": user.user_notification_activity_count,
                  "mail_count": user.user_notification_mail_count,
                  "deal_count": user.user_notification_deal_count,
                  "lead_count": user.user_notification_lead_count
               }
              io.emit('notification_' + userId, {
                  type: 'newmail',
                  message: 'New Mail',
                  data: mail,
                  notification:notification
              })
              io.emit('newmail_' + userId, {
                  message: 'New Mail',
                  data: mail
              })
              return mailId
            }
            else reject("Failred to save mail")
              }
              else reject("Failred to save mail")
        }  else {
          console.log("Found, Saving canceled")
          resolve(null)
        }
    })
}

function removeEmojis (string) {
    var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  
    return string.replace(regex, '');
  }
module.exports = {
    setImap,
    FetchMails,
    FetchMailsFirst
  }
  