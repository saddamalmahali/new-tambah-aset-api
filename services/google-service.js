'use strict'

const Fs = require('fs')
const Path = require('path')
const Crypto = require('crypto')
const Google = require('./google-api')
const {google} = require('googleapis');
const userMailsAttachmentRepo = require('../repositories/user_mails_attachments')
const userMailsRepo = require('../repositories/user_mails')
const usersRepo = require('../repositories/users')
const userMailsPersonsRepo = require('../repositories/user_mails_persons')
var commonHelper = require('../helpers/common')
// const { log, error } = require('./log')

async function exchangeCodeForToken ({ client, code }) {
  if (!client) throw new Error('Missing "client" arg. Requires a Google API client.')
  if (!code) throw new Error('Missing "code" arg. Requires a Google auth code to be exchanged for a token.')

  const token = await Google.auth.exchangeCodeForToken(client, code)
  return token
//   const file = process.env.GSS_TOKEN_CACHE_FILE
//   Fs.writeFileSync(file, JSON.stringify(token, null, 2))
//   log(`Wrote token to ${file}`)
}

async function printAuthUrl (client) {
  console.log("CLIENT : ",client)
  return Google.auth.getAuthUrl(client)
}
async function watchMyLabel(auth) {
  const gmail = google.gmail({
      version: 'v1',
      auth
  });
  const res = await gmail.users.watch({
      userId: 'me',
      requestBody: {
        //   labelIds: ['UNREAD'],
          labelFilterAction: "include",
          topicName: process.env.GSS_GOOGLE_TOPIC_NAME
      }
  });
  return res
}
async function setWatch(client){
    return new Promise(async(resolve, reject)=>{
      if(typeof client!=='undefined'){
        if(client!=null){
        var result = await watchMyLabel(client)
        // result  = await watchMyLabel(client)
        // console.log("Result Watch", result)
        if (result && result.data.expiration) {
            // console.log("RESULTNYA : ", resultnya)
            resolve(true)
        } else {
            reject("Failed to save watch")
        }
      } else reject("Failed to set watch")
      } else reject("Failed to set watch")
    })
}
async function fetchMessages (client,userId, sync_type, time, messageId="") {
    return new Promise(async (resolve, reject)=>{
        // console.log("ISI OPTS", opts)
        if(!client) reject('Missing "client" arg. Requires a Google API client.')
        if(!sync_type) reject('Missing "sync_type" arg. Requires a Google API client.')
        if(!time) reject('Missing "time" arg. Requires a Google API client.')
        if(!userId) reject('Missing "userId" arg. Requires a Google API client.')
        let result
        let unfilteredMessageIds = []
        var q = ""
        // Setup Gmail watch for the token owner’s inbox.
        // console.log("FETCHING MESSAGES")
        // List messages for the given email address.
        // NOTE: If opts.only is an email address, we’ll filter messages to only those
        // sent to / received from that email address.
        // 'now','one_month','two_month','three_month','four_month'
        // `from:${email} OR to:${email}`
        var now = new Date(time)
        var minus = 1
        if(sync_type=='one_month') minus=1
        if(sync_type=='two_month') minus=2
        if(sync_type=='three_month') minus=4
        if(sync_type=='four_month') minus=4

        var MonthBefore = now
        MonthBefore.setMonth(MonthBefore.getMonth() - minus)
        MonthBefore.setHours(0, 0, 0);
        MonthBefore.setMilliseconds(0)
        var year = MonthBefore.getFullYear()
        var month = MonthBefore.getMonth()+1
        var date = MonthBefore.getDate()
        MonthBefore = year + '/' + month +'/'+ date;
        var year = now.getFullYear() 
        var month = now.getMonth()+1
        var date = now.getDate()
        now = year + '/' + month +'/'+ date;
        if(sync_type=='now') q = 'after:'+(now)
        if(sync_type=='one_month' || sync_type=='two_month' || sync_type=='three_month' || sync_type=='four_month') q = 'before:'+(MonthBefore)
        if(messageId!='')
        result = await Google.gmail.listMessages(client,'','me', 1)
        else
        result = await Google.gmail.listMessages(client,q)
        // await watchMyLabel(client)
        console.log("Month Before : ", MonthBefore)
        if(result){
          if(result.data.resultSizeEstimate>0){
        // console.log("RESULT MESSAGES ", result)
        if (!result || !result.data.messages) reject('Error to Get List Messages.')
        unfilteredMessageIds = result.data.messages.map(x => x.id)


        // Filter out messages that we’ve already fetched.
        const messageIds = await filterExistingMessages(unfilteredMessageIds)

        // log(`Kept ${messageIds.length} / ${unfilteredMessageIds.length} messages.`)
        if (!messageIds.length) return

        const messages = []
        let count = 0

        // Fetch messages.

        for (const messageId of messageIds) {
            // log(`${++count}. Fetching message ${messageId} ..`)
             result = await Google.gmail.getMessage(client, messageId)
            const message = await getMessage(result, userId)
            messages.push(message)
        }

        // Fetch attachments.
        await fetchAttachments(client, messages, time)

        var promises = []
        if(messageIds.length>0){
          // Save messages.
          messages.map(async message=>{
            console.log("Saving Message : ", message)
            promises.push(new Promise(async (resolve, reject)=>{
              saveMessage(userId,message, time).then(res=>{
                resolve(res)
              })
            }))
          })
          Promise.all(promises).then(ret=>{
            console.log("FINAL RESULT : ", ret)
            resolve(ret)
          }).catch(e=>{
            console.log("EROR NGEWE",e)
            reject("Failed to fetch messages")
          })
        } else {
          resolve(true)
        }
      } else resolve(true)
      } else reject(false)
    })
}

async function handleGoogleNotification (json) {
  if (json && json.message && json.subscription) {
    const d = JSON.parse(Buffer.from(json.message.data, 'base64').toString())
    const data = {
      _id: json.message.messageId.toString(),
      email: d.emailAddress,
      historyId: d.historyId,
      subscription: json.subscription,
      created: new Date(json.message.publishTime)
    }
    return data
  } return false
}

async function getWatchByEmail (db, email) {
  var collection = await db.collection('gss_watch')
    var result = await collection.findOne({ email })
  return result
}

function saveWatch (db, userId, data) {
  // console.log("Saving Watch")
  data.expiration = parseInt(data.expiration, 10)
  // log(`Saving Gmail watch for ${data.email} (id: ${userId}) expiring on ${new Date(data.expiration)}`)
  return db.collection('gss_watch').findOneAndUpdate(
    { _id: userId },
    { $set: data },
    { upsert: true } // upsert === true NOT upsert === 1 !
  )
}

async function getLatestMessage (db, userId) {
  
  // console.log("Getting Latest message of ", userId)
  var collection = await db.collection('gss_messages')
    var result = await collection
      .find()
      .sort({$natural: -1})
      .limit(1)
      .next()
    return result
}

async function checkAttachmentContentHash (contentId) {
  return new Promise(async (resolve, reject)=>{
    var where = {
      user_mails_attachment_contentId : contentId
    }
    var find = await userMailsAttachmentRepo.findByWhere(where)
    if(find) resolve(true)
    else resolve(false)
  })
}

async function saveAttachmentContentHash (contentId, messageId, other = { }) {
  return new Promise(async (resolve, reject)=>{
    var dataCreate = {
      user_mails_attachment_filename : other.originalFilename,
      user_mails_attachment_url: other.filename,
      user_mails_attachment_mail_id: messageId,
      user_mails_attachment_created_time: other.time,
      user_mails_attachment_contentId: other.contentId
    }
    var flag = await userMailsAttachmentRepo.create(dataCreate)
    if(flag){

    } else {

    }
    const c = db.collection('gss_attachment_content')
    const exists = await c.findOne({ _id: contentId })
    if (!exists) {
      return c.insertOne(Object.assign({ _id: contentId, messages: [messageId] }, other))
    }
    return c.findOneAndUpdate({ _id: contentId }, { $addToSet: { messages: messageId } })
  })
}



async function saveMessage (userId,message, time) {
    new Promise(async (resolve, reject)=>{
        var where = {
          user_mail_mailId : message._id
        }
        var found = await userMailsRepo.findByWhere(where)
        console.log("WHERE found", found)
        if(found.length==0){
          console.log("Not Found, Saving Message")
          var where   = {
            user_mails_person_email : message.fromEmail,
            user_mails_person_user_id : userId
          }
          var person = await userMailsPersonsRepo.findOneByWhere(where)
            //insert
            var dataCreate = {
                user_mail_user_id : userId,
                user_mail_sender_email : message.fromEmail,
                user_mail_sender_name : message.fromName,
                user_mail_content : message.snippet,
                user_mail_receiver_name : message.toName,
                user_mail_receiver_email : message.toEmail,
                user_mail_snippet : message.snippet,
                user_mail_historyId : message.historyId,
                user_mail_date : (time!=null?new Date(parseFloat(message.date)):new Date()),
                user_mail_body: message.body,
                user_mail_labelIds: message.labelIds.toString(),
                user_mail_subject: removeEmojis(message.subject.toString()),
                user_mail_mailId: message._id,
                user_mail_threadId: message.threadId,
                user_mail_created_time: time,
                user_mail_person_id: (person?person.user_mails_person_person_id:null)
            }
            console.log("Mail Data : ", dataCreate)
            var res = await userMailsRepo.create(dataCreate)
            var mailId = res.user_mail_id
            console.log("MAIL ID", mailId)
            var mail    = await userMailsRepo.findById(mailId)
            console.log("MAIL INSIDE", mail)
            if(mail ){
                var where       = {
                    user_mails_attachment_mail_id : mail.user_mail_id
                }
                var attachments = await userMailsAttachmentRepo.findByWhere(where)
                mail.setDataValue('attachments', attachments)
            if(mail) {
              resolve(mail)
              console.log("Emitting Socket", mail)
              await commonHelper.create_notification(userId,'NEW_MAIL', mail.user_mail_id, time)
              var user = await usersRepo.findById(userId)
              var notification = {
                  "total_count": user.user_notification_total_count,
                  "activity_count": user.user_notification_activity_count,
                  "mail_count": user.user_notification_mail_count,
                  "deal_count": user.user_notification_deal_count,
                  "lead_count": user.user_notification_lead_count
               }
              io.emit('notification_' + userId, {
                  type: 'newmail',
                  message: 'New Mail',
                  data: mail,
                  notification:notification
              })
              io.emit('newmail_' + userId, {
                  message: 'New Mail',
                  data: mail
              })
              return mailId
            }
            else reject("Failred to save mail")
              }
              else reject("Failred to save mail")
        }  else {
          console.log("Found, Saving canceled")
          resolve(null)
        }
    })
}
function removeEmojis (string) {
  var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;

  return string.replace(regex, '');
}
function cleanText(body){
  body = body.replace(/\u00E1/g,"a");  //LATIN SMALL LETTER A WITH ACUTE
body = body.replace(/\u00E2/g,"a");  //LATIN SMALL LETTER A WITH CIRCUMFLEX
body = body.replace(/\u00E3/g,"a");  //LATIN SMALL LETTER A WITH TILDE
body = body.replace(/\u201D/g,"\"");  //RIGHT DOUBLE QUOTATION MARK
body = body.replace(/\u201C/g,"\"");  //LEFT DOUBLE QUOTATION MARK
body = body.replace(/\u2424/g," ");  //NEW LINE \n
body = body.replace(/\u000D/g," ");  //CARRIAGE RETURN \r
  return body
}
function searchBodyRec(payload){
  if (payload.body && payload.body.size>0) {
      return payload.body.data;
  } else if (payload.parts && payload.parts.length) {
    console.log("PARTS PAYLOAD : ", payload.parts)
    var body =payload.parts.find((x)=>(x.mimeType.toString()=="text/html"))
    console.log("BODY FOUND ", body)
    if(typeof body!='undefined')
      return body.body.data
      else return false
  }
}

async function filterExistingMessages(messageIds) {
  return new Promise(async (resolve, reject)=>{
    var where = {
      user_mail_threadId: messageIds
    }
    const messages = await userMailsRepo.findByWhere(where)
    if(messages)
    resolve(messageIds.filter(x => !messages.find(y => y.user_mail_threadId === x)))
    else resolve(messageIds)
  })
}

function getHash (str) {
  const hash = Crypto.createHash('sha256')
  hash.update(str)
  return hash.digest('hex')
}

function createFile (originalFilename, base64Data) {
  const filename = Path.resolve('./assets/uploads/', getHash(base64Data) + Path.extname(originalFilename))
  const data = Buffer.from(base64Data, 'base64')
  Fs.writeFileSync(filename, data)
  // log(`Saved attachment: "${originalFilename}" (${data.length} bytes) to ${filename}`)
  return filename
}

async function fetchAttachments (client, messages, time) {
  const attachmentsMap = messages.reduce((acc, m) => {
    if (m.parts) {
      m.parts
      .filter(x => x.attachmentId)
      .forEach(x => {
        acc[x.attachmentId] = x
      })
    }
    return acc
  }, { })

  let downloaded = 0
  let identical = 0

  for (const attachmentId of Object.keys(attachmentsMap)) {
    const part = attachmentsMap[attachmentId]
    const existing = await checkAttachmentContentHash( part.contentId)
    if (existing) {
      part.localFile = existing.localFile
      identical++
    } else {
      // Download the attachment.
      const attachment = await Google.gmail.getAttachment(client, attachmentId, part.messageId)
      downloaded++
      if (attachment.data) {
        part.localFile = createFile(part.filename, attachment.data)
      }
    }
    part.attachmentId = 'downloaded'

    const metadata = {
      localFile: part.localFile,
      originalFilename: part.filename,
      messageId: part.messageId,
      time: time
    }
    await saveAttachmentContentHash(part.contentId, part.messageId, metadata)
  }

  // log(`Downloaded ${downloaded} / ${Object.keys(attachmentsMap).length} attachments (identical: ${identical}).`)
}

async function getMessage (m, userId) {
  if(m.data.payload){
    // console.log("PAYLOADNYA: ",m.data.payload)
  const [fromName, fromEmail] = m.data.payload.headers.find(x => x.name === 'From').value.replace(/"/g, '').split(' <');
  const subject = m.data.payload.headers.find(x => x.name === 'Subject').value
  console.log("SUBJECTNYA ", subject)
  var toName = ""
  var toEmail = ""
  // var subject = ""
  m.data.payload.headers.map(x=>{
    // if(x.name=='Subject'){
    //   subject = x.value
    // }
    if(x.name=='To') {
      console.log("ORIGINAL TO ", x.value)
      var flag = x.value.indexOf('<')
      if(flag!=-1){
        toName = x.value.substr(0, x.value.indexOf('<')-1);
        toEmail = x.value.substr(x.value.indexOf('<')+1,x.value.indexOf('>'));
      } else {

        toName = x.value
        toEmail = x.value
      }
    }
  })
  // console.log("PARTS", m.data.payload.parts)
  // const [toName, toEmail] = m.data.payload.headers.find(x => x.name === 'To').value.split(' <')
var body =  await searchBodyRec(m.data.payload)
if(typeof body!='undefined' && body!=false)
 body =   Buffer.from(body, 'base64').toString('utf-8')
 else {console.log("BODY NOT FOUND ",m.data.payload)
 body=""
}
  const message = {
    _id: m.data.id,
    userId,
    threadId: m.data.threadId,
    labelIds: m.data.labelIds,
    snippet: m.data.snippet,
    historyId: m.data.historyId,
    date: m.data.internalDate,
    fromName,
    fromEmail: (fromEmail?fromEmail.replace('>', ''):""),
    toName,
    toEmail: toEmail.replace('>', ''),
    body:body,
    subject: subject
  }
// if(m.data.payload.parts!=null){
//   message.parts = m.data.payload.parts.map((acc, x) => {
//     if (x.filename && x.body && x.body.attachmentId) {
//       acc.push({
//         userId,
//         messageId: message._id,
//         partId: x.partId,
//         mimeType: x.mimeType,
//         filename: x.filename,
//         size: x.body.size,
//         attachmentId: x.body.attachmentId,
//         contentId: x.headers.find(x => x.name === 'X-Attachment-Id').value
//       })
//     }

//     if (x.parts) {
//       x.parts.forEach(y => {
//         // Skip html parts.
//         if (y.mimeType === 'text/html') return
//         acc.push({
//           partId: y.partId,
//           mimeType: y.mimeType,
//           body: Buffer.from(y.body.data || '', 'base64').toString('utf8'),
//           size: y.body.size
//         })
//       })
//     }
//     return acc
//   }, [])
// }
console.log("MESSAGE : ",message)
  return message
} else return false
}

async function setupDb (db) {
  // await db.ensureIndex('gss_messages', { userId: 1, fromEmail: 1 })
  // await db.ensureIndex('gss_messages', { userId: 1, toEmail: 1 })
  // await db.ensureIndex('gss_watch', { email: 1 })
  // log('Google Gmail sync: setup db indexes.')
}

module.exports = {
  printAuthUrl,
  exchangeCodeForToken,
  fetchMessages,
  handleGoogleNotification,
  setupDb,
  setWatch,
  getMessage,
  saveMessage
}
