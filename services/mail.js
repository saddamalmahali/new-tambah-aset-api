const mailerHelper = require('../helpers/mailer')
const Queue = require('bee-queue')
var handlebars = require('hbs')
const fs = require('fs')
var readHTMLFile = function (path, callback) {
    fs.readFile(path, {
        encoding: 'utf-8'
    }, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        } else {
            callback(null, html);
        }
    });
}
const options = {
    redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
    },
}
//Create Register Queue
global.reqisterQueue = new Queue('RegisterMail', options)
global.resetQueue = new Queue('ResetPasswordMail', options)
global.otpQueue = new Queue('OTP', options)

exports.runService = function(req, res){
    console.log('Mail Service is running')
    //Processing Register Queue
    reqisterQueue.process(function (job, done) {
        console.log(`Processing job ${job.id}`);

        console.log("processing queue")
        //send invitation email
        readHTMLFile('./assets/html/email_verification.html', function (err, html) {
            var template = handlebars.compile(html);
            var replacements = {
                web_url: process.env.BASE_URL,
                code: job.data.code,
                confirm_link: job.data.confirm_link,
                year: job.data.year,
            }
            var htmlToSend = template(replacements);
            var subject = "Motra Account Verification"
            mailerHelper.send_mail(process.env.COMPANY_EMAIL, job.data.email_to, subject, htmlToSend).then(async function (ret) {
                if (ret) {
                    return done()
                } else {
                    return done()
                }
            })
        })
      })
    //Processing Reset Queue
    resetQueue.process(function (job, done) {
        console.log(`Processing job ${job.id}`);

        console.log("processing queue")
        //send invitation email
        readHTMLFile('./assets/html/reset_password.html', function (err, html) {
            console.log("reading html tempalte")
            var template = handlebars.compile(html);
            var replacements = {
                web_url: process.env.BASE_URL,
                new_password: job.data.new_password,
                year: job.data.year,
            }
            var htmlToSend = template(replacements);
            var subject = "Motra Account Reset Password"
            mailerHelper.send_mail(process.env.COMPANY_EMAIL, job.data.email_to, subject, htmlToSend).then(async function (ret) {
                if (ret) {
                    console.log('Processing Queue success')
                    return done()
                } else {
                    console.log('Processing Queue failed')
                    return done()
                }
            })
        })
      })
    //Processing OTP Queue
    otpQueue.process(function (job, done) {
        console.log(`Processing job ${job.id}`);

        console.log("processing queue")
        //send invitation email
        readHTMLFile('./assets/html/otp.html', function (err, html) {
            console.log("reading html tempalte")
            var template = handlebars.compile(html);
            var replacements = {
                web_url: process.env.BASE_URL,
                otp: job.data.code,
                year: job.data.year,
            }
            var htmlToSend = template(replacements);
            var subject = "Motra One Time Password"
            mailerHelper.send_mail(process.env.COMPANY_EMAIL, job.data.email_to, subject, htmlToSend).then(async function (ret) {
                if (ret) {
                    console.log('Processing Queue success')
                    return done()
                } else {
                    console.log('Processing Queue failed')
                    return done()
                }
            })
        })
      })
}
