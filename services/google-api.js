
const {OAuth2Client} = require('google-auth-library');
const {google} = require('googleapis');
require('dotenv').config()
console.log(process.env.GSS_GOOGLE_CREDENTIALS)
const credentials = require(process.env.GSS_GOOGLE_CREDENTIALS)
const MailComposer = require('nodemailer/lib/mail-composer');
// const { error } = require('./log')

const SCOPES = [
  'https://www.googleapis.com/auth/drive.metadata.readonly',
  'https://www.googleapis.com/auth/plus.me',
  'https://mail.google.com/',
  'email',
  'profile',
]

function getClient (token) {
  const clientSecret = credentials.web.client_secret
  const clientId = credentials.web.client_id
  const redirectUrl = credentials.web.redirect_uris[0]

  console.log(clientSecret, clientId,redirectUrl )
  const client = new OAuth2Client(clientId, clientSecret, redirectUrl)

  client.credentials = token

  return client
}

async function getClientRefreshToken(refreshToken){
  const clientSecret = credentials.web.client_secret
  const clientId = credentials.web.client_id
  const redirectUrl = credentials.web.redirect_uris[0]

  console.log(clientSecret, clientId,redirectUrl )
  const client = new OAuth2Client(clientId, clientSecret, '')

  // client.setCredentials({
  //     refresh_token: token
  // });
  await client.setCredentials({refresh_token: refreshToken  })
  const newAccessToken = await client.refreshAccessToken()
  console.log("ACCESS", newAccessToken.res)
  return newAccessToken.res.data
}

const auth = {
  verifyIdToken (client, idToken) {
    return new Promise((resolve, reject) => {
      client.verifyIdToken(idToken, credentials.web.client_id, (err, login) => {
        if (err) return reject(err)
        resolve(login.getPayload())
      })
    })
  },

  getAuthUrl (client) {
    return client.generateAuthUrl({
      access_type: 'offline', // Required to get refresh token.
      scope: SCOPES
    })
  },

  exchangeCodeForToken (client, code) {
    return new Promise((resolve, reject) => {
      client.getToken(code, (err, token) => {
        if (err) {
          // error('Error while trying to retrieve access token', err)
          return reject(err)
        }
        client.credentials = token
        resolve(token)
      })
    })
  }
}

const drive = {
  listFiles (client) {
    return new Promise((resolve, reject) => {
      const service = google.drive('v3')
      service.files.list({
        auth: client,
        pageSize: 10,
        fields: 'nextPageToken, files(id, name)'
      }, getApiResponseHandler(resolve, reject))
    })
  }
}

const plus = {
  getProfile (client, userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.people('v1').people.get({
        resourceName: 'people/me',
        personFields: 'emailAddresses,names',
        auth: client
      }, getApiResponseHandler(resolve, reject))
    })
      // const service = google.plus('v1')
      // service.people.get({
      //   auth: client,
      //   userId
      // }, getApiResponseHandler(resolve, reject))
    // })
  }
}

const gmail = {
  watchInbox (client, topicName, labelIds='',userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      var options = {
        auth: client,
        userId,
        // Use the resource key to add a payload to the request body when doing an HTTP POST.
        resource: { topicName },
        labelFilterAction: 'include',
      }
      if(labelIds!='')
      options.labelIds = labelIds
      service.users.watch(options, getApiResponseHandler(resolve, reject))
    })
  },
  sendMessage(auth, raw) {
    return new Promise((resolve, reject)=>{
      const service = google.gmail('v1')
      service.users.messages.send({
          auth: auth,
          userId: 'me',
          resource: {
              raw: raw
          }
      }, getApiResponseHandler(resolve, reject));
    })
},
makeBody( from,to, subject, message, attachments="") {
  return new Promise((resolve, reject)=>{
    // attachment format     
    //  [
    //   {   // encoded string as an attachment
    //     filename: 'text1.txt',
    //     content: 'aGVsbG8gd29ybGQh',
    //     encoding: 'base64'
    //   },
    //   {   // encoded string as an attachment
    //     filename: 'text2.txt',
    //     content: 'aGVsbG8gd29ybGQh',
    //     encoding: 'base64'
    //   },
    // ]
    var opts = {
      to: to,
      from:from,
      text:message,
      html: message,
      subject: subject,
      textEncoding: "base64",
    }
    if(attachments!="")
    opts.attachments = attachments
    console.log("Options Mail Body : ", opts)
    let mail = new MailComposer(opts);
      mail.compile().build( (error, msg) => {
        if (error) {
           console.log('Error compiling email ' + error)
           reject('Error compiling email ' + error)
        }
        const encodedMessage = Buffer.from(msg)
          .toString('base64')
          .replace(/\+/g, '-')
          .replace(/\//g, '_')
          .replace(/=+$/, '');
        resolve(encodedMessage)
      })
  })
},
  removeLabel(client,emailId='', labelIds=''){
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      var options = {
        auth: client,
        userId:'me',
        'id':emailId,
        // Use the resource key to add a payload to the request body when doing an HTTP POST.
        resource: {
          addLabelIds:[],
          removeLabelIds: labelIds
      },
      }
      service.users.messages.modify(options, getApiResponseHandler(resolve, reject))
    })
  },
   deleteBatch(client, list) {
    return new Promise( (resolve, reject) => {
      const service = google.gmail('v1')
      var options = {
        auth: client,
        userId: "me",
        resource: { ids: list },
      }
      service.users.messages.batchDelete(options,getApiResponseHandler(resolve, reject));
    })
  },
  addLabel(client,emailId='', labelIds=''){
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      var options = {
        auth: client,
        userId:'me',
        'id':emailId,
        // Use the resource key to add a payload to the request body when doing an HTTP POST.
        resource: {
          addLabelIds:labelIds
      },
      }
      service.users.messages.modify(options, getApiResponseHandler(resolve, reject))
    })
  },

  listThreads (client, email = null, userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      const opts = {
        auth: client,
        userId
      }
      if (email) {
        opts.q = `from:${email} OR to:${email}`
      }
      service.users.threads.list(opts, getApiResponseHandler(resolve, reject))
    })
  },

  listHistory (client, startHistoryId, userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      service.users.history.list({
        auth: client,
        startHistoryId,
        userId
      }, getApiResponseHandler(resolve, reject))
    })
  },

  listMessages (client, query = null, userId = 'me', maxResults='') {
    return new Promise((resolve, reject) => {
      console.log("CLIENT : ", client)
      const service = google.gmail('v1')
      const opts = {
        auth: client,
        userId
      }
      if(maxResults!='') opts.maxResults = maxResults
      if (query) {
        opts.q = query
      }
      console.log(opts)
      service.users.messages.list(opts, getApiResponseHandler(resolve, reject))
    })
  },

  getThread (client, threadId, userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      service.users.threads.get({
        auth: client,
        id: threadId,
        userId
      }, getApiResponseHandler(resolve, reject))
    })
  },

  getMessage (client, messageId, userId = 'me') {
    return new Promise((resolve, reject) => {
      console.log(messageId)
      const service = google.gmail('v1')
      service.users.messages.get({
        auth: client,
        id: messageId,
        userId
      }, getApiResponseHandler(resolve, reject))
    })
  },

  getAttachment (client, attachmentId, messageId, userId = 'me') {
    return new Promise((resolve, reject) => {
      const service = google.gmail('v1')
      service.users.messages.attachments.get({
        auth: client,
        id: attachmentId,
        messageId,
        userId
      }, getApiResponseHandler(resolve, reject))
    })
  }
}

function getApiResponseHandler (resolve, reject) {
  return (err, response) => {
    if (err) {
      // error('The API returned an error:', err)
      return reject(err)
    }
    resolve(response)
  }
}

module.exports = {
  getClient,
  auth,
  drive,
  gmail,
  plus,
  getClientRefreshToken
}
