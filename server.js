//By Fikri akhdi
const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
dotenv.config()
var cors = require('cors')
const fs = require('fs')
var path = require('path')
const helmet = require('helmet')
var compression = require('compression')
const morgan = require('morgan')
// var redis = require('redis')
var hbs = require('express-handlebars')
const mongoose = require('mongoose')
// const url = `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`;


// global.client = redis.createClient({
//   url,
//   password: process.env.REDIS_PASSWORD
// });

global.reconnectingEvent = false
// client.connect();
global.passport = require('passport')

const passportJWT = require('passport-jwt')
// const usersRepo = require('./repositories/users')
const passport = require('passport');
// const mailService = require('./services/mail')
var apiHelper = require('./helpers/api')

global.appRoot = path.resolve(__dirname);

// Config Database
// ---------------------------------------
const mongo_uri = process.env.MONGO_URI || '';
mongoose.connect(mongo_uri, { autoIndex: false }).then(() => {
  console.log("Successfully connected to database");
}).catch((error) => {
  console.log("database connection failed. exiting now...");
});

//Connecting Redis Client to Server
// client.on('ready', async () => {
//   console.log('client ready')
// })
// client.on('connect', async function () {
//   console.log('Redis client connected')
//   await mailService.runService()
// })
// client.on('error', function (err) {
//   console.log('Something went wrong ' + err)
// })
// client.on('reconnecting', error => {
//   console.log('client reconnecting', error)
//   reconnectingEvent = true
// })


// Setting JWT
// ==========================================
// let ExtractJwt = passportJWT.ExtractJwt;
// let JwtStrategy = passportJWT.Strategy;
// let jwtOptions = {};
// jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
// jwtOptions.secretOrKey = process.env.JWT_SECRET_KEY;

// // set JWT strategy
// let strategy = new JwtStrategy(jwtOptions, function (jwt_payload, next) {
//   // let user = usersRepo.findById(jwt_payload.id)
//   // if (user) {
//   //   next(null, user)
//   // } else {
//   //   next(null, false)
//   // }
// })


// use the strategy
// passport.use(strategy)

var app = require('express')()

var myRawParser = function (req, res, next) {
  req.rawData = ''
  if (req.header('content-type') == 'text/plain') {
    req.on('data', function (chunk) {
      req.rawData += chunk
    })
    req.on('end', function () {
      next()
    })
  } else {
    next()
  }
}

app.use(myRawParser)

// enabling compression for faster process
// ---------------------------------------
app.use(compression())

// enabling CORS for all requests
// ---------------------------------------
app.use(cors())

// adding Helmet to enhance your API's security
// ---------------------------------------
app.use(helmet())
app.use(express.json({
  limit: '100mb'
}));

app.use(express.urlencoded({
  limit: '100mb'
}));

//views engine for testing purpose only
// ---------------------------------------
app.set('view engine', 'hbs')

//Passport setup
// ---------------------------------------
// app.use(passport.initialize());
// app.use(passport.session());

// app.use((req, res, next) => {
//   res.type('json')
//   // res.header("Access-Control-Allow-Origin", "*");
//   // res.header(
//   //     "Access-Control-Allow-Headers",
//   //     "Origin, X-Requested-With, Content-Type, Accept, Authorization"
//   // );
//   // if (req.method === "OPTIONS") {
//   //     res.header("Access-Control-Allow-Methods", "POST,  DELETE, GET");
//   //     return res.status(200).json({});
//   // }
//   next();
// });

// passport.serializeUser(function (user, cb) {
//   cb(null, user);
// });

// passport.deserializeUser(function (obj, cb) {
//   cb(null, obj);
// });

// // create a write stream (in append mode)
// var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
//   flags: 'a'
// })



// var error = fs.createWriteStream(path.join(__dirname, 'error.log'), { flags: 'a' });
// adding morgan to log HTTP requests
// app.use(morgan('combined', { stream: accessLogStream }))

// initialize passport with express
// ---------------------------------------
app.use(passport.initialize())

// parse application/json
// ---------------------------------------
app.use(bodyParser.json())

// parse application/x-www-form-urlencoded
// ---------------------------------------
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(function (err, req, res, next) {

  if (err.name === 'UnauthorizedError') {
    res.status(err.status).send(apiHelper.api_format(false, 'Something went wrong', err.status));
    return;
  }

  next();
})
require('./routes')(app)

const port = process.env.PORT

var server = app.listen(port, "0.0.0.0")

global.io = require('socket.io')(server);

io.on('connection', function (socket) {
  console.log('connected socket!');

  socket.on('greet', function (data) {
    console.log(data);
    socket.emit('respond', { hello: 'Hey, Mr.Client!' });
  });
  socket.on('disconnect', function () {
    console.log('Socket disconnected');
  });

});

module.exports = server //for testing