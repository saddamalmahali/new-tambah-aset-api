// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const Otp_model = require('../models/otp')
// const Otp = Otp_model(sequelize, Sequelize)

// //Sync the model
// Otp.sync()

// //Model Function Otp List
// Otp.list = function (limit=null) {
//   var options = {}
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Otp.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// };

// //Model Function Find data by ID
// Otp.findById = function (id) {
//   return new Promise(resolve => {
//         Otp.findOne({
//           where: {
//             id: id
//           }
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else {
//             client.set("otp:" + id, JSON.stringify(ret))
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//   })
// }
// //Model Function Find data by where
// Otp.findByWhere = function (where, limit=null) {
//   var options = {
//     where:where
//   }
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Otp.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model function delete data
// Otp.deleteByID = function(id){
//   return new Promise(resolve => {
//           Otp.destroy({where: {id:id}}).then(ret=>{
//               if(ret) {
//                 client.del("otp:" + id)
//                 resolve(true)
//               }
//               else resolve(false)
//           })
//   })
//   .catch(err => {
//     console.log(err)
//     resolve(null)
//   })
// }

// // Model Function Update Group Data
// Otp.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Otp.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//         Otp.findOne({where:{
//           id:id
//         }}).then(ret=>{
//           client.set("otp:" + id, JSON.stringify(ret))
//           resolve(ret)
//         })
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }
// module.exports = Otp