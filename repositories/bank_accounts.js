// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const BankAccounts_model = require('../models/bank_accounts')
// const BankAccounts = BankAccounts_model(sequelize, Sequelize)
// const Banks_model = require('../models/banks')
// const Users_model = require('../models/users')
// const Banks = Banks_model(sequelize, Sequelize)
// const Users = Users_model(sequelize, Sequelize)

// //Connect the relations
// BankAccounts.hasOne(Banks, {
//   through: 'bank_id',
//   foreignKey: 'id',
//   sourceKey: 'bank_id'
// })
// BankAccounts.hasOne(Users, {
//   through: 'created_by',
//   foreignKey: 'id',
//   sourceKey: 'created_by'
// })
// //Sync the model
// BankAccounts.sync()
// Banks.sync()

// //Model Function BankAccounts List
// BankAccounts.list = function (code="") {
//   return new Promise(resolve => {
//     BankAccounts.findAll({
//       include: [Banks,{
//         model:Users,
//         as: "user",
//         attributes: ['name', 'email', 'phone_number']
//       }]
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model Function BankAccounts Single
// BankAccounts.findById = function (id="") {
//   return new Promise(resolve => {
//         BankAccounts.findOne({
//           where: {
//             id: id
//           },
//             include: [Banks,{
//               model:Users,
//               as: "user",
//               attributes: ['name', 'email', 'phone_number']
//             }]
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else{
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//   })
// }

// //Model Function Find data by where
// BankAccounts.findByWhere = function (where,groupBy=null, limit=null) {
//     var options = {
//       where: where,
//         include: [Banks,{
//           model:Users,
//           as: "user",
//           attributes: ['name', 'email', 'phone_number']
//         }]
//     }
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//     if(groupBy!=null)
//     options.group =  groupBy
//     if (limit != null) {
//       options.offset = limit.offset
//       options.limit = limit.limit
//     }
//     return new Promise(resolve => {
//       BankAccounts.findAll(options).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//     })
//   }

// // Model Function Update Data
// BankAccounts.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     BankAccounts.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = BankAccounts