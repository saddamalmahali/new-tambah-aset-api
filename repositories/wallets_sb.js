// const Sequelize = require('sequelize')
// const {
//   Op
// } = require('sequelize')
// const sequelize = require('../sequelize')
// const Wallets_model = require('../models/wallets_sb')
// const Companies_model = require('../models/companies')
// const Wallets = Wallets_model(sequelize, Sequelize)
// const Companies = Companies_model(sequelize, Sequelize)

// //Connect the relations
// Wallets.hasOne(Companies, {
//   through: 'company_id',
//   foreignKey: 'id',
//   sourceKey: 'company_id'
// })

// //Sync the model
// Wallets.sync()

// //Model Function Balance
// Wallets.balance = function (company_id='') {
//   return new Promise(resolve => {
//     if(company_id!=''){
//     sequelize.query(`SELECT IFNULL(current_total,0) as current_total FROM wallets_sbs WHERE company_id=:company_id ORDER BY date_created DESC LIMIT 1`, {
//         replacements: {
//           company_id: company_id,
//         },
//         type: sequelize.QueryTypes.SELECT
//       })
//       .then(result => {
//         resolve(result)
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//     } else {
//       sequelize.query(`SELECT IFNULL(current_total,0) as current_total FROM wallets_sbs ORDER BY date_created DESC LIMIT 1`,{
//           type: sequelize.QueryTypes.SELECT
//         })
//         .then(result => {
//           resolve(result)
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//     }
//   })
// }

// //Model Function Find data by ID
// Wallets.findById = function (id) {
//   return new Promise(resolve => {
//       Wallets.findOne({
//         where: {
//           id: id
//         },
//         include: [Companies]
//       }).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// //Model Function Find data by where
// Wallets.findByWhere = function (where,groupBy=null, limit=null) {
//   var options = {
//     where: where,
//     include: [Companies]
//   }
//   if(groupBy!=null)
//   options.group =  groupBy
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//   }
//   return new Promise(resolve => {
//     Wallets.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// // Model Function Update User Data
// Wallets.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Wallets.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = Wallets
