// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const ApiDocumentations_model = require('../models/api_documentations')
// const ApiDocumentations = ApiDocumentations_model(sequelize, Sequelize)

// //Sync the model
// ApiDocumentations.sync()

// //Model Function ApiDocumentations List
// ApiDocumentations.list = function () {
//   return new Promise(resolve => {
//     ApiDocumentations.findAll({
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model Function ApiDocumentations Single
// ApiDocumentations.findById = function (id="") {
//   return new Promise(resolve => {
//         ApiDocumentations.findOne({
//           where: {
//             id: id
//           },
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else{
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//   })
// }

// //Model Function Find data by where
// ApiDocumentations.findByWhere = function (where,groupBy=null, limit=null) {
//     var options = {
//       where: where,
//     }
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//     if(groupBy!=null)
//     options.group =  groupBy
//     if (limit != null) {
//       options.offset = limit.offset
//       options.limit = limit.limit
//     }
//     return new Promise(resolve => {
//       ApiDocumentations.findAll(options).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//     })
//   }

// // Model Function Update Data
// ApiDocumentations.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     ApiDocumentations.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = ApiDocumentations