// const Sequelize = require('sequelize')
// const {
//   Op
// } = require('sequelize')
// const sequelize = require('../sequelize')
// const TransactionDetails_model = require('../models/transaction_details_sb')
// const Transactions_model = require('../models/transactions_sb')
// const TransactionDetails = TransactionDetails_model(sequelize, Sequelize)
// const Transactions = Transactions_model(sequelize, Sequelize)

// //Connect the relations
// TransactionDetails.hasOne(Transactions, {
//   through: 'transaction_id',
//   foreignKey: 'id',
//   sourceKey: 'transaction_id'
// })

// //Sync the model
// TransactionDetails.sync()


// //Model Function Find data by ID
// TransactionDetails.findById = function (id) {
//   return new Promise(resolve => {
//       TransactionDetails.findOne({
//         where: {
//           transaction_id: id
//         },
//         include: [Transactions]
//       }).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// //Model Function Find data by where
// TransactionDetails.findByWhere = function (where,groupBy=null, limit=null) {
//   var options = {
//     where: where,
//     include: [Transactions]
//   }
//   if(groupBy!=null)
//   options.group =  groupBy
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//   }
//   return new Promise(resolve => {
//     TransactionDetails.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// // Model Function Update Data
// TransactionDetails.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     TransactionDetails.update(data, {
//         where: {
//             transaction_id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = TransactionDetails
