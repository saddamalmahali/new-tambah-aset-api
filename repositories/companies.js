// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const Companies_model = require('../models/companies')
// const Companies = Companies_model(sequelize, Sequelize)

// //Sync the model
// Companies.sync()

// //Model Function Companies List
// Companies.list = function () {
//   return new Promise(resolve => {
//     Companies.findAll({
//       where: {'is_deleted':null}}).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model Function Companies Single
// Companies.findById = function (id="") {
//   return new Promise(resolve => {
//     client.get("company:" + id, function (err, reply) {
//       if (err) console.log(err)
//       if (!reply) {
//         Companies.findOne({
//           where: {
//             id: id
//           }
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else{
//             client.set("company:" + id, JSON.stringify(ret))
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//       } else
//       resolve(JSON.parse(reply))
//     })
//   })
// }
// //Model Function Find data by where
// Companies.findByWhere = function (where,groupBy=null, limit=null) {
//   var options = {
//     where: where,
//   }
//   if(groupBy!=null)
//   options.group =  groupBy
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//   }
//   return new Promise(resolve => {
//     Companies.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }
// // Model Function Update Data
// Companies.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Companies.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = Companies