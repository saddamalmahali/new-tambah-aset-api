// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
const Coins = require('../models/coins')
const moment = require('moment')
// const {
//   Op
// } = require('sequelize')
// const Coins = Coins_model(sequelize, Sequelize)

// //Sync the model
// Coins.sync()

//Model Function Coins List
// ----------------------------------------------
Coins.list = function (limit = null) {
    var options = {}

    let limitData = 10
    let offset = 0

    if (limit != null) {
        offset = limit.offset
        limitData = limit.limit
    }
    return new Promise(resolve => {
        Coins.find(options)
            .limit(limitData)
            .skip(offset).then(function (ret) {
                if (!ret) {
                    resolve(null)
                } else
                    resolve(ret)
            }).catch(err => {
                console.log(err)
                resolve(null)
            })
    })
};

//Model Function Find data by ID
// ----------------------------------------------
Coins.findById = function (id) {
    return new Promise(resolve => {
        Coins.findById(id).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            console.log(err)
            resolve(null)
        })
    })
}

// Model Function Find Latest Data
// ----------------------------------------------
Coins.findLatestUpdate = function (indicator) {
    return new Promise(resolve => {
        Coins.findOne({
            indicator: indicator
        }).sort({ date_created: 'desc' }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            resolve(null)
        })
    })
}

// Coins.findLatestUpdate = function () {
//   return new Promise(resolve => {
//         Coins.findOne({
//           order: [ [ 'date_created', 'DESC' ]],
//       }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else {
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//   })
// }

Coins.findLatestPrice = function (symbol) {
    return new Promise(resolve => {
        Coins.findOne({ symbol }).sort({ 'date_created': 'desc' }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            console.log(err)
            resolve(null)
        })
    })
}

// Model Function Find data by where
// -----------------------------------
Coins.findLatestCoins = function (limit) {

    // if(search!="" && search!=null){
    //   options.where.symbol = search
    // }
    // if(limit && search==''){
    //   options.limit = limit
    // } else {
    //   options.limit = 1400
    // }

    return new Promise(resolve => {
        Coins.find({}).limit(limit).sort({ date_created: 'desc' }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            resolve(err.message)
        })
    })
}
// Coins.findLatestCoins = function (limit) {
//   var options = {
//     limit: limit,
//     // where: {
//     //   interval
//     // },
//     order: [ [ 'date_created', 'DESC' ]],
//   }
//   // if(search!="" && search!=null){
//   //   options.where.symbol = search
//   // }
//   // if(limit && search==''){
//   //   options.limit = limit
//   // } else {
//   //   options.limit = 1400
//   // }
//   return new Promise(resolve => {
//     Coins.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(options)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }

// Model Function Find data by where
// -----------------------------------
// Coins.findSpecificCoin = function (coin, interval) {
//   var options = {
//     limit: 140,
//     where: {
//       symbol: coin,
//       interval
//     },
//     order: [ [ 'date_created', 'DESC' ]],
//   }
//   // if(search!="" && search!=null){
//   //   options.where.symbol = search
//   // }
//   // if(limit && search==''){
//   //   options.limit = limit
//   // } else {
//   //   options.limit = 1400
//   // }
//   return new Promise(resolve => {
//     Coins.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(options)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }

// Model Function Find data by where
// -----------------------------------

// Coins.findLatestCoins = function (limit, interval='') {
//   var options = {
//     limit: limit,
//     where: {
//       interval
//     },
//     order: [ [ 'date_created', 'DESC' ]],
//   }
//   // if(search!="" && search!=null){
//   //   options.where.symbol = search
//   // }
//   // if(limit && search==''){
//   //   options.limit = limit
//   // } else {
//   //   options.limit = 1400
//   // }
//   return new Promise(resolve => {
//     Coins.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(options)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }

// Model Function Find last 5 min data
// -----------------------------------
Coins.findPreviousData = function (coin, interval) {

    if (interval == '5m') {
        interval = '5m'
    } else if (interval == '60m') {
        interval = '60m'
    } else if (interval == '240m') {
        interval = '240m'
    }

    // var d = new Date();
    // d.setMinutes(d.getMinutes() - 1);
    var d = moment().subtract(1, 'minute').format("YYYY-MM-DD hh:mm:ss");

    var options = {
        date_created: { $gte: d },
        symbol: coin,
        interval: interval
    }
    return new Promise(resolve => {
        Coins.findOne(options).sort({ date_created: 'desc' }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            resolve(null)
        })
    })
}
// Coins.findPreviousData = function (coin, interval) {
//   if(interval=='5m'){
//     interval = '5'
//   } else if(interval=='60m'){
//     interval = '60'
//   } else if(interval=='240m'){
//     interval = '240'
//   }
//   var options = {
//     where: {
//       date_created: {
//         [Op.gte]: sequelize.literal(`DATE_SUB(NOW(), INTERVAL ${interval} MINUTE)`)
//       },
//       symbol: coin,
//     },
//     order: [ [ 'date_created', 'DESC' ]],
//   }
//   return new Promise(resolve => {
//     Coins.findOne(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(null)
//     })
//   })
// }

// Model Function Find data for last 100 minutes
// -----------------------------------
// const now = new Date();
// const serverTimezoneOffset = now.getTimezoneOffset() * 60 * 1000;
// const last100Mins = new Date(now - 100 * 60 * 1000 - serverTimezoneOffset);
// Coins.findLast100M = function(){
//   var opt = {
//     // where: {
//     //   date_created: {
//     //     [Op.gte]: last100Mins
//     //   }
//     // },
//     limit : 2079224,
//     order: [['date_created', 'DESC']]
//   }
//   return new Promise(resolve => {
//     Coins.findAll(opt).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }

// Model Function Find suggestion by where
// -----------------------------------
Coins.getSuggestion = function (where) {
    var options = {
        ...where
    }
    return new Promise(resolve => {

        Coins.aggregate([
            { $match: options },
            {
                $group: {
                    _id: "symbol"
                },
            },
            { $sort: { "symbol": 1 } },
        ]).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            resolve(err.message)
        })
    })
}

// Coins.findHourlyCoins = function () {
//   return new Promise(resolve => {
//     Coins.findAll({
//       attributes: [
//         [Sequelize.fn('AVG', Sequelize.col('value')), 'value'],
//         [Sequelize.fn('DATE_FORMAT', Sequelize.col('date_created'), '%Y-%m-%d %H'), 'hour'],
//         'symbol',
//       ],
//       where: {
//         date_created: {
//           [Op.gte]: Sequelize.literal('DATE_SUB(NOW(), INTERVAL 24 HOUR)'),
//         },
//       },
//       group: ['symbol', 'hour'],
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(options)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }

// Coins.findForthlyCoins = function () {
//   return new Promise(resolve => {
//     Coins.findAll({
//       attributes: [
//         [Sequelize.fn('AVG', Sequelize.col('value')), 'value'],
//         [Sequelize.fn('DATE_FORMAT', Sequelize.col('date_created'), '%Y-%m-%d %H:00:00'), 'hour'],
//         'symbol',
//       ],
//       where: {
//         date_created: {
//           [Op.gte]: Sequelize.literal('DATE_SUB(NOW(), INTERVAL 24 HOUR)'),
//         },
//       },
//       group: [
//         'symbol',
//         Sequelize.literal('HOUR(date_created) DIV 4'),
//       ],
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(options)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       resolve(err.message)
//     })
//   })
// }


// Model Function Find data by where
// -------------------------------------------------
Coins.findByWhere = function (where, limit = null) {

    var options = {
        ...where
    }

    let limitData = 10
    let offset = 0

    if (limit != null) {
        offset = limit.offset
        limitData = limit.limit
    }

    return new Promise(resolve => {
        Coins.find(options).limit(limitData).skip(offset).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => {
            console.log(err)
            resolve(null)
        })
    })
}


// //Model function delete data
// Coins.deleteByID = function(id){
//   return new Promise(resolve => {
//           Coins.destroy({where: {id:id}}).then(ret=>{
//               if(ret) {
//                 client.del("coins:" + id)
//                 resolve(true)
//               }
//               else resolve(false)
//           })
//   })
//   .catch(err => {
//     console.log(err)
//     resolve(null)
//   })
// }
// Coins.deleteCoins = function(){
//   return new Promise(resolve => {
//           Coins.destroy({where: {}}).then(ret=>{
//               if(ret) {
//                 resolve(true)
//               }
//               else resolve(false)
//           })
//   })
//   .catch(err => {
//     console.log(err)
//     resolve(null)
//   })
// }

// // Model Function Update Group Data
// Coins.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Coins.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//         Coins.findOne({where:{
//           id:id
//         }}).then(ret=>{
//           client.set("coins:" + id, JSON.stringify(ret))
//           resolve(ret)
//         })
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

module.exports = Coins