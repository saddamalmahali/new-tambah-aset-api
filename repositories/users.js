// const Sequelize = require('sequelize')
// const {
//   Op
// } = require('sequelize')
// const sequelize = require('../sequelize')
// const Users_model = require('../models/users')
// const Role_model = require('../models/roles')
// const Companies_model = require('../models/companies')
// const Users = Users_model(sequelize, Sequelize)
// const Role = Role_model(sequelize, Sequelize)
// const Companies = Companies_model(sequelize, Sequelize)

// //Connect the relations
// Users.hasOne(Companies, {
//   through: 'company_id',
//   foreignKey: 'id',
//   sourceKey: 'company_id'
// })
// Users.hasOne(Role, {
//   through: 'role_id',
//   foreignKey: 'id',
//   sourceKey: 'role_id'
// })
// Role.belongsToMany(Users, {
//   through: 'role_id'
// })
// Companies.belongsToMany(Users, {
//   through: 'company_id'
// })

// //Sync the model
// Users.sync()
// Role.sync()

// //Model Function Banks List
// Users.list = function () {
//   return new Promise(resolve => {
//     Users.findAll({
//       where: {'is_deleted':null},
//       include: [Role,Companies]}).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model Function Authentication
// Users.authentication = function (username, password, remoteDevice, remoteIP) {

//   return new Promise(resolve => {
//     sequelize.query(`CALL _SpLogin(:username, :password, :remoteDevice, :remoteIP, @Check);SELECT @Check;`, {
//         replacements: {
//           username: username,
//           password: password,
//           remoteDevice: remoteDevice,
//           remoteIP: remoteIP
//         },
//         type: sequelize.QueryTypes.SELECT
//       })
//       .then(res => {
//         resolve(res)
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// //Model Function Find data by ID
// Users.findById = function (user_id) {
//   return new Promise(resolve => {
//       Users.findOne({
//         where: {
//           id: user_id
//         },
//         include: [Role,Companies]
//       }).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// //Model Function Find data by where
// Users.findByWhere = function (where,groupBy=null, limit=null) {
//   var options = {
//     where: where,
//     include: [Role,Companies]
//   }
//   if(groupBy!=null)
//   options.group =  groupBy
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//     options.order =  [
//       ['date_created', 'DESC']
//     ]
//   }
//   return new Promise(resolve => {
//     Users.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model function to check Username Validation
// Users.userNameValidation = function (username) {
//   return new Promise(resolve => {
//     //tolower string
//     username = username.toLowerCase()
//     Users.findOne({
//       where: {
//         email: username
//       }
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(true)
//       } else
//         resolve(false)
//     }).catch(err => {
//       console.log(err)
//       resolve(false)
//     })
//   })
// }

// //Model function to check Username Validation
// Users.selfUserNameValidation = function (id, username) {
//   return new Promise(resolve => {
//     //tolower string
//     username = username.toLowerCase()
//     Users.findOne({
//       where: {
//         email: username,
//         id: { [Op.not]: id}
//       }
//     }).then(function (ret) {
//       if (!ret) {
//         resolve(true)
//       } else{
//         resolve(false)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(false)
//     })
//   })
// }

// // Model Function Update User Data
// Users.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Users.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = Users