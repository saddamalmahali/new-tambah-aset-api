// const Sequelize = require('sequelize')
// const {
//   Op
// } = require('sequelize')
// const sequelize = require('../sequelize')
// const Transactions_model = require('../models/transactions_sb')
// const Transaction_details_model = require('../models/transaction_details_sb')
// const Users_model = require('../models/users')
// const Transactions = Transactions_model(sequelize, Sequelize)
// const Transaction_details = Transaction_details_model(sequelize, Sequelize)
// const Users = Users_model(sequelize, Sequelize)

// //Connect the relations
// Transactions.hasMany(Transaction_details, {
//   foreignKey: 'transaction_id',
// })
// Transactions.hasOne(Users, {
//   through: 'user_id',
//   foreignKey: 'id',
//   sourceKey: 'user_id'
// })
// Transactions.hasOne(Users, {
//     through: 'created_by',
//     foreignKey: 'id',
//     sourceKey: 'created_by'
//   })

// //Sync the model
// Transactions.sync()

// //Model Function Find data by ID
// Transactions.findById = function (id) {
//   return new Promise(resolve => {
//       Transactions.findOne({
//         where: {
//           id: id
//         },
//         include: [{
//           model:Users,
//           as: "user",
//           attributes: ['name', 'email', 'phone_number']
//         }]
//       }).then(function (ret) {
//         if (!ret) {
//           resolve(null)
//         } else {
//           resolve(ret)
//         }
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// //Model Function Find data by where
// Transactions.findByWhere = function (where,groupBy=null, limit=null) {
//   var options = {
//     where: where,
//     include: [{
//       model:Users,
//       as: "user",
//       attributes: ['name', 'email', 'phone_number']
//     },Transaction_details]
//   }
//   options.order =  [
//     ['date_created', 'DESC']
//   ]
//   if(groupBy!=null)
//   options.group =  groupBy
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Transactions.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }
// //Model Function Num Of
// Transactions.num_of = function (type='',status='') {
//     return new Promise(resolve => {
//         var qry = `SELECT IFNULL(COUNT(id),0) as total FROM transactions_sbs WHERE 1 `
//         if(type!='') qry+=" AND type = '"+type+"'"
//         if(status!='') qry+=" AND status = '"+status+"'"
//       sequelize.query(qry, {
//           type: sequelize.QueryTypes.SELECT
//         })
//         .then(result => {
//           resolve(result)
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//     })
//   }

//   //Model Function Transaction Amount
//   Transactions.transaction_amount = function (type='',status='') {
//   return new Promise(resolve => {
//       var qry = `SELECT IFNULL(SUM(total_nominal),0) as total FROM transactions_sbs WHERE 1 `
//       if(type!='') qry+=" AND type = '"+type+"'"
//       if(status!='') qry+=" AND status = '"+status+"'"
//     sequelize.query(qry, {
//         type: sequelize.QueryTypes.SELECT
//       })
//       .then(result => {
//         resolve(result)
//       }).catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

//   //Model Function Transaction Amount
//   Transactions.get_trails = function (type,status) {
//     return new Promise(resolve => {

//         var qry = ''
//         if(type=='count') qry = `SELECT IFNULL(COUNT(id),0) AS total, DATE(date_created) AS date FROM transactions_sbs WHERE 1 AND year(date_created)=:year AND status=:status GROUP BY date(date_created)`
//         else qry = `SELECT IFNULL(SUM(total_nominal),0) AS total, DATE(date_created) AS date FROM transactions_sbs WHERE 1 AND year(date_created)=:year AND status=:status GROUP BY date(date_created)`

//         console.log(qry)
//       sequelize.query(qry,{replacements: {year:new Date().getFullYear(), status:status},
//           type: sequelize.QueryTypes.SELECT
//         })
//         .then(result => {
//           resolve(result)
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//     })
//   }


// // Model Function Update User Data
// Transactions.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Transactions.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//           resolve(ret)
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }

// module.exports = Transactions
