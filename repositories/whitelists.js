// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const Whitelists_model = require('../models/whitelists')
// const Whitelists = Whitelists_model(sequelize, Sequelize)
// const Users_model = require('../models/users')
// const Users = Users_model(sequelize, Sequelize)

// Whitelists.hasOne(Users, {
//   through: 'created_by',
//   foreignKey: 'id',
//   sourceKey: 'created_by'
// })
// //Sync the model
// Whitelists.sync()

// //Model Function Whitelists List
// Whitelists.list = function (limit=null) {
//   var options = {include:[{
//     model:Users,
//     as: "user",
//     attributes: ['name', 'email', 'phone_number']
//   }]}
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Whitelists.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// };

// //Model Function Find data by ID
// Whitelists.findById = function (id) {
//   return new Promise(resolve => {
//     client.get("whitelists:" + id, function (err, reply) {
//       if (err) console.log(err)
//       if (!reply) {
//         Whitelists.findOne({
//           where: {
//             id: id
//           },include:[{
//             model:Users,
//             as: "user",
//             attributes: ['name', 'email', 'phone_number']
//           }]
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else {
//             client.set("whitelists:" + id, JSON.stringify(ret))
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//       } else
//         resolve(JSON.parse(reply))
//     })
//   })
// }
// //Model Function Find data by where
// Whitelists.findByWhere = function (where, limit=null) {
//   var options = {
//     where:where,
//     include:[{
//       model:Users,
//       as: "user",
//       attributes: ['name', 'email', 'phone_number']
//     }]
//   }
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Whitelists.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model function delete data
// Whitelists.deleteByID = function(id){
//   return new Promise(resolve => {
//           Whitelists.destroy({where: {id:id}}).then(ret=>{
//               if(ret) {
//                 client.del("whitelists:" + id)
//                 resolve(true)
//               }
//               else resolve(false)
//           })
//   })
//   .catch(err => {
//     console.log(err)
//     resolve(null)
//   })
// }

// // Model Function Update Group Data
// Whitelists.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Whitelists.update(data, {
//         where: {
//           id: id
//         }, transaction
//       }).then(ret => {
//         Whitelists.findOne({where:{
//           id:id
//         }}).then(ret=>{
//           client.set("whitelists:" + id, JSON.stringify(ret))
//           resolve(ret)
//         })
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }
// module.exports = Whitelists