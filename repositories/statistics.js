const Statistics = require('../models/statistics')

//Model Function Statistics List
// ---------------------------------------
Statistics.list = function (limit = null) {
    var options = {}

    let offset = 0
    let limitData = 10
    if (limit != null) {
        offset = limit.offset || 0
        limitData = limit.limit
    }

    return new Promise(resolve => {
        Statistics.find({}).limit(limitData).skip(offset).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else
                resolve(ret)
        }).catch(err => { resolve(null) })
    })
};

// Model Function Find data by ID
// ---------------------------------------
Statistics.findById = function (id) {
    return new Promise(resolve => {
        Statistics.findOne({ _id: id }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => { resolve(null) })
    })
}

// Model Function Find data by Symbol
// ---------------------------------------
Statistics.findBySymbol = function (symbol) {
    return new Promise(resolve => {
        Statistics.findOne({ symbol: symbol, }).sort({ date: 'desc' }).then(function (ret) {
            if (!ret) {
                resolve(null)
            } else {
                resolve(ret)
            }
        }).catch(err => { resolve(null) })
    })
}

// Model Function Find data by where
// ---------------------------------------
Statistics.findByWhere = function (where, limit = null) {
    var options = {
        ...where
    }

    let offset = 0;
    let limitData = 10;

    if (limit != null) {
        if (limit.offset) {
            offset = limit.offset
        } else if (limit.limit) {
            limitData = limit.limit
        }
    }

    return new Promise(resolve => {
        Statistics.find(options)
            .limit(limitData)
            .skip(offset)
            .sort({ date: 'desc' })
            .then(function (ret) {
                if (!ret) {
                    resolve(null)
                } else {
                    resolve(ret)
                }
            }).catch(err => {
                resolve(null)
            })
    })
}

// // Model function delete data
// Statistics.deleteByID = function (id) {
//   return new Promise(resolve => {
//     Statistics.destroy({ where: { id: id } }).then(ret => {
//       if (ret) {
//         client.del("statistics:" + id)
//         resolve(true)
//       }
//       else resolve(false)
//     })
//   })
//     .catch(err => {
//       console.log(err)
//       resolve(null)
//     })
// }

// // Model Function Update Group Data
// Statistics.updateData = function (id, data, transaction = null) {
//   return new Promise(resolve => {
//     Statistics.update(data, {
//       where: {
//         id: id
//       }, transaction
//     }).then(ret => {
//       Statistics.findOne({
//         where: {
//           id: id
//         }
//       }).then(ret => {
//         client.set("statistics:" + id, JSON.stringify(ret))
//         resolve(ret)
//       })
//     })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }


// Statistics.deleteStatistics = function () {
//   return new Promise(resolve => {
//     Statistics.destroy({ where: {} }).then(ret => {
//       if (ret) {
//         resolve(true)
//       }
//       else resolve(false)
//     })
//   })
//     .catch(err => {
//       console.log(err)
//       resolve(null)
//     })
// }


module.exports = Statistics