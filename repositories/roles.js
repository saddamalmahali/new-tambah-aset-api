// const Sequelize = require('sequelize')
// const sequelize = require('../sequelize')
// const Roles_model = require('../models/roles')
// const Roles = Roles_model(sequelize, Sequelize)

// //Sync the model
// Roles.sync()

// //Model Function Roles List
// Roles.list = function (limit=null) {
//   var options = {}
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Roles.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else
//         resolve(ret)
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// };

// //Model Function Find data by ID
// Roles.findById = function (role_id) {
//   return new Promise(resolve => {
//     client.get("roles:" + role_id, function (err, reply) {
//       if (err) console.log(err)
//       if (!reply) {
//         Roles.findOne({
//           where: {
//             role_id: role_id
//           }
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else {
//             client.set("roles:" + role_id, JSON.stringify(ret))
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//       } else
//         resolve(JSON.parse(reply))
//     })
//   })
// }//Model Function Find data by Slug and CompanyId
// Roles.findBySlug = function (role_slug, role_company_id) {
//   return new Promise(resolve => {
//     client.get("roles:" + role_slug+role_company_id, function (err, reply) {
//       if (err) console.log(err)
//       if (!reply) {
//         Roles.findOne({
//           where: {
//             role_slug: role_slug,
//             role_company_id: role_company_id
//           }
//         }).then(function (ret) {
//           if (!ret) {
//             resolve(null)
//           } else {
//             client.set("roles:" + role_slug+role_company_id, JSON.stringify(ret))
//             resolve(ret)
//           }
//         }).catch(err => {
//           console.log(err)
//           resolve(null)
//         })
//       } else
//         resolve(JSON.parse(reply))
//     })
//   })
// }
// //Model Function Find data by where
// Roles.findByWhere = function (where, limit=null) {
//   var options = {
//     where:where
//   }
//   if (limit != null) {
//     options.offset = limit.offset
//     options.limit = limit.limit
//   }
//   return new Promise(resolve => {
//     Roles.findAll(options).then(function (ret) {
//       if (!ret) {
//         resolve(null)
//       } else {
//         resolve(ret)
//       }
//     }).catch(err => {
//       console.log(err)
//       resolve(null)
//     })
//   })
// }

// //Model function delete data
// Roles.deleteByID = function(id){
//   return new Promise(resolve => {
//           Roles.destroy({where: {role_id:id}}).then(ret=>{
//               if(ret) {
//                 client.del("roles:" + id)
//                 resolve(true)
//               }
//               else resolve(false)
//           })
//   })
//   .catch(err => {
//     console.log(err)
//     resolve(null)
//   })
// }

// // Model Function Update Group Data
// Roles.updateData = function (id, data, transaction=null) {
//   return new Promise(resolve => {
//     Roles.update(data, {
//         where: {
//           role_id: id
//         }, transaction
//       }).then(ret => {
//         Roles.findOne({where:{
//           role_id:id
//         }}).then(ret=>{
//           client.set("roles:" + id, JSON.stringify(ret))
//           resolve(ret)
//         })
//       })
//       .catch(err => {
//         console.log(err)
//         resolve(null)
//       })
//   })
// }
// module.exports = Roles