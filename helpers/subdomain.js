
const axios = require('axios')
module.exports = {
    create_domain: async function (domain) {
        return new Promise(async (resolve, reject)=>{
            var url = process.env.CLOUDFLARE_API_URL
            console.log("URL "+url)
            var headers = {
                "X-Auth-Email":process.env.CLOUDFLARE_API_EMAIL,
                "X-Auth-Key":process.env.CLOUDFLARE_API_KEY
            }
            console.log("Headers : ", headers)
            var data = {
                "type":"A",
                "name":domain,
                "content":"185.81.165.100",
                "ttl":0,
                "priority":10,
                "proxied":true}
          
          console.log("Data : ", data)
             axios({
                method: 'post',
                url: url,
                headers: headers, 
                data: data}).then(response=>{
                    resolve(response)
                }).catch((error) => {
                    // console.log(error)
                    resolve(null)
                });;
        })
    }
}