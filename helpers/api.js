module.exports = {
    api_format: function (success = "false", message = "", error_code = "", data = null,data_count=null) {
        var data_json = {
            success: success,
            message: message
        }
        if (error_code != "") data_json.error_code = error_code
        if (data != null) data_json.data = data
        if (data_count != null) data_json.data_count = data_count
        return JSON.stringify(data_json)
    }
}