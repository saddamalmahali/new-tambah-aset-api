const companiesFieldDataRepo = require('../repositories/companies_field_data')
const modulesPermissionsGroupsRepo = require('../repositories/modules_permissions_groups')
const personsRepo = require('../repositories/persons')
const organizationsRepo = require('../repositories/organizations')
const companiesFieldAccessRepo = require('../repositories/companies_field_access')
const companiesFieldRepo = require('../repositories/companies_fields')
const currenciesRepo = require('../repositories/currencies')
const usersRepo = require('../repositories/users')
module.exports = {
    test: function(){
        console.log("test")
    },
    getStaticFields: async function(scope){
        return new Promise((resolve, reject)=>{
            var where = {
                companies_field_scope : scope,
                companies_field_is_default: true
            }
            companiesFieldRepo.findByWhere(where).then(ret=>{
                resolve(ret)
            })
        })
    },
    getCustomFieldDataAll: async function (id, type) {
        return new Promise((resolve, reject)=>{
            var where = {
                companies_field_data_group_id:id
            }
            companiesFieldDataRepo.findByWhere(where).then(ret=>{
                if(ret!=null && typeof ret!="undefined" && ret.length>0){
                    console.log("Custom Field Found")
                    var custom_fields = []
                    ret.map(async custom_field=>{
                            var value =""
                            if(custom_field.companies_field_data_type=='Person'){
                                value = await personsRepo.findById(custom_field.companies_field_data_person_id)
                            }else if(custom_field.companies_field_data_type=='Organization'){
                                value = await organizationsRepo.findById(custom_field.companies_field_data_organization_id)
                            }else if(custom_field.companies_field_data_type=='Currency'){
                                value = await currenciesRepo.findById(custom_field.companies_field_data_currency_id)
                            }else {
                                value = custom_field.companies_field_data_value
                            }
                            custom_fields.push({
                                key: custom_field.companies_field.companies_field_key,
                                title:custom_field.companies_field.companies_field_title,
                                value:value
                            })
                    })
                    resolve(custom_fields)
                } else {
                    console.log("No Custom Fields")
                    resolve(null)
                }
            })
        })
    },
    getCustomFieldData: async function (user_role_id,id, type) {
        return new Promise((resolve, reject)=>{
            var where = {
            }
            if(type=="deal"){
                where.companies_field_data_deal_id = id
            } else if(type=="person"){
                where.companies_field_data_person_id = id
            } else if(type=="product"){
                where.companies_field_data_product_id = id
            } else if(type=="organization"){;''
                where.companies_field_data_organization_id = id
            } else if(type=="lead"){
                where.companies_field_data_lead_id = id
            } else if(type=="currency"){
                where.companies_field_data_currency_id = id
            }
            //get static fields
                companiesFieldDataRepo.findByWhere(where).then(async custom_field=>{
                    if(custom_field!=null && typeof custom_field!="undefined" && custom_field.length>0){
                        console.log("Custom Field Found")
                            var promises = []
                            custom_field.map(o=>{
                                promises.push(new Promise(async (resolve, reject)=>{
                                    var value = ""
                                    if(o.companies_field_data_type=='Person'){
                                        value = await personsRepo.findById(o.companies_field_data_person_id)
                                    }else if(o.companies_field_data_type=='Organization'){
                                        value = await organizationsRepo.findById(o.companies_field_data_organization_id)
                                    }else if(o.companies_field_data_type=='Currency'){
                                        value = await currenciesRepo.findById(o.companies_field_data_currency_id)
                                    }else {
                                        value = o.companies_field_data_value
                                    }
                                    var module_permission_id = o.companies_field.companies_field_module_permission_id
                                    var where = {
                                        modules_permissions_group_module_permissions_id : module_permission_id,
                                        modules_permissions_group_group_id : user_role_id
                                    }
                                    var modules_permissions_groups = await modulesPermissionsGroupsRepo.findByWhere(where)             
                                    if(modules_permissions_groups){
                                        var where = {
                                            companies_field_access_field_id : o.companies_field.companies_field_id
                                        }
                                        var access = await companiesFieldAccessRepo.findByWhere(where)
                                        o.setDataValue('access', access)
                                        resolve(o)
                                } else resolve(null)
                                }))
                                Promise.all(promises).then(ret=>{
                                    resolve(ret)
                                }).catch(e=>{
                                    console.log(e)
                                    resolve(null)
                                })
                            })
                    } else {
                        console.log("No Custom Fields" )
                        resolve(null)
                    }
                })
            })
    },
    createCustomFieldData: async function(type, id, value, user_id,data_id, time, transaction=""){
        return new Promise(async(resolve, reject)=>{
            var dataCreate = {
                companies_field_data_value: value,
                companies_field_data_user_id: user_id,
                companies_field_data_field_id: id,
                companies_field_data_person_id: (type=='person'?data_id:null),
                companies_field_data_deal_id: (type=='deal'?data_id:null),
                companies_field_data_organization_id: (type=='organization'?data_id:null),
                companies_field_data_product_id: (type=='product'?data_id:null),
                companies_field_data_lead_id: (type=='lead'?data_id:null),
                companies_field_data_currency_id: (type=='currency'?data_id:null),
                companies_field_data_created_time: time,
            }
            console.log("DATA CREATE : ", dataCreate)
            if(transaction!=""){
                var ret = await companiesFieldDataRepo.create(dataCreate,{transaction})
                resolve(ret)
            } else {
                var ret = await companiesFieldDataRepo.create(dataCreate)
                resolve(ret)
            }
        })
    },
    updateCustomFieldData: async function(field_id,type, id, value, user_id,data_id, time, transaction=""){
        return new Promise(async(resolve, reject)=>{
            var dataUpdate = {
                companies_field_data_id: field_id,
                companies_field_data_value: value,
                companies_field_data_user_id: user_id,
                companies_field_data_field_id: id,
                companies_field_data_person_id: (type=='person'?data_id:null),
                companies_field_data_deal_id: (type=='deal'?data_id:null),
                companies_field_data_organization_id: (type=='organization'?data_id:null),
                companies_field_data_product_id: (type=='product'?data_id:null),
                companies_field_data_lead_id: (type=='lead'?data_id:null),
                companies_field_data_currency_id: (type=='currency'?data_ind:null),
                companies_field_data_updated_time: time,
            }
            console.log("Update Custom Field", dataUpdate)
            if(transaction!=""){
                var ret = await companiesFieldDataRepo.updateData(parseInt(field_id), dataUpdate,transaction)
                resolve(ret)
            } else {
                var ret = await companiesFieldDataRepo.updateData(parseInt(field_id,dataUpdate))
                resolve(ret)
            }
        }).catch(e=>{
            console.log(e)
        })
    }
}
