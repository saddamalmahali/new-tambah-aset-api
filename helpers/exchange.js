
const exchangeRatesRepo = require('../repositories/exchange_rates')
const axios = require('axios')
module.exports = {
    updateExchangeRates: async function () {
  return new Promise(resolve => {
      var exchangeURL = process.env.CURRENCY_EXCHANGE_API_URL
      var date = moment().format('YYYY-MM-DD')
      console.log("URL EXCHANGE : " + exchangeURL + date)
      var promises = []
      exchangeRatesRepo.findByWhere({}).then(ret => {
          if (ret > 0) {
              //get latest rate by API
              axios.get(exchangeURL + date).then(resp => {
                  if (resp.data) {
                      console.log(resp.data)
                      const keys = Object.keys(resp.data.rates)
                      for (const key of keys) {
                          promises.push(new Promise((resolve, reject) => {
                              var dataUpdate = {
                                  exchange_rate_code: key,
                                  exchange_rate_value: parseFloat(resp.data.rates[key]).toFixed(2),
                                  exchange_rate_update_time: new Date()
                              }
                              console.log(dataUpdate.exchange_rate_value)
                              exchangeRatesRepo.updateData(key, dataUpdate).then(ret => {
                                  if (ret) resolve(dataUpdate)
                                  else resolve(null)
                              })
                          }))
                      }

                      Promise.all(promises).then(function (results) {
                          resolve(results)
                      }).catch(e => {
                          resolve(null)
                      })
                  } else resolve(null)
              })
          } else {
              //get latest rate by API
              axios.get(exchangeURL + date).then(resp => {
                  if (resp.data) {
                      console.log(resp.data)
                      const keys = Object.keys(resp.data.rates)
                      for (const key of keys) {
                          promises.push(new Promise((resolve, reject) => {
                              var dataCreate = {
                                  exchange_rate_code: key,
                                  exchange_rate_value: parseFloat(resp.data.rates[key]).toFixed(2),
                                  exchange_rate_create_time: new Date()
                              }
                              console.log(dataCreate.exchange_rate_value)
                              exchangeRatesRepo.create(dataCreate).then(ret => {
                                  if (ret) resolve(dataCreate)
                                  else resolve(null)
                              })
                          }))
                      }

                      Promise.all(promises).then(function (results) {
                          resolve(results)
                      }).catch(e => {
                          resolve(null)
                      })
                  } else resolve(null)
              })
          }
      })
  })
}
}