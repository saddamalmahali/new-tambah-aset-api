
const userNotificationsRepo = require('../repositories/user_notifications')
module.exports = {
    username_validation: function (username) {
        username = username.toString()
        //check for the criteria
        var format = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/)
        if (username.length < 6 || username.length > 254 || username == null || format.test(username || username!="")) {
            return false;
        } else {
            return true;
        }
    },
    parse_validation: function (errors) {
        console.log(errors)
        var ret = ""
        Object.keys(errors).forEach(function (key, i) {
            ret += key.toUpperCase() + " : " + errors[key].message + " "
        })
        return ret
    },
    deal_status: function(string){
        //0=> default, 1=> lost, 2=> won, 3=> delete
        var ret = ""
        if(string=="0") ret = "Active"
        if(string=="1") ret = "Lost"
        if(string=="2") ret = "Won"
        if(string=="3") ret = "Delete"
        return ret
    },
    password_validation: function(string){
        // Validate lowercase letters
        var lowerCaseLetters  = new RegExp(/[a-z]/g)
        // Validate capital letters
        var upperCaseLetters  = new RegExp(/[A-Z]/g)
        // Validate numbers
        var numbers = /\d/g
        if(!lowerCaseLetters.test(string) || !upperCaseLetters.test(string) || !numbers.test(string) || string.length<8)
        {
            var message = []
            if(!lowerCaseLetters.test(string)) message.push("You have to use lower case")
            if(!upperCaseLetters.test(string)) message.push("You have to use upper case")
            if(!numbers.test(string)) message.push("You have to use numbers")
            if(string.length<8) message.push("At least 8 characters")
            return message
        }
        else
        return true 
    },
    create_notification: async function(userId,type,referId,time){
        return new Promise(async(resolve, reject)=>{
            var dataCreate = {
                user_notification_user_id : userId,
                user_notification_is_unread: true,
                user_notification_created_time: time,
                user_notification_type: type,
                user_notification_deal_id: (type=="DEAL"?referId:null),
                user_notification_lead_id: (type=="LEAD"?referId:null),
                user_notification_activity_id: (type=="ACTIVITY"?referId:null),
                user_notification_mail_id: (type=="NEW_MAIL"?referId:null),
            }
            var res = await userNotificationsRepo.create(dataCreate)
            if(res) resolve(res)
            else reject("Failed to create")
        })
    }
}