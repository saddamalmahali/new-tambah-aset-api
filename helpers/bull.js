var Bull = require('bull')
module.exports = {
    createQueue: function (queueName, queueOptions) {
        return new Bull(queueName,queueOptions)
    },
    addJob: async function (queue, jobData, jobOptions) {
        return await queue.add(jobData, jobOptions)
    }
}