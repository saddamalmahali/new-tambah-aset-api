
var smtpTransport = require('nodemailer-smtp-transport')
var nodemailer = require('nodemailer')
var transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD
  },
  secureConnection: 'false',
  tls: {
    ciphers: 'SSLv3',
    rejectUnauthorized: false
  }
})
module.exports = {
    send_mail: function (from = "", to = "", subject = "", emailTemplate = "", attachments=null) {
        return new Promise(resolve => {
            var mailOptions = {
                from: from,
                to: to,
                subject: subject,
                html: emailTemplate
              }
              console.log("Sending Email to "+to)
              if(attachments!=null) mailOptions.attachments = attachments
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                  console.log(error)
                    resolve(false)
                } else {
                  resolve(true)
                }
              })
        })
    }
}