
const axios = require('axios')
var qs = require('qs')
const crypto = require("crypto")
const randtoken = require('rand-token')
const banksRepo = require('../repositories/banks')
const moment = require("moment")
module.exports = {
    getToken: async function () {
        return new Promise(async resolve => {
            try{
            var host = process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_DEV_HOST:process.env.PERMATA_HOST
            var url = process.env.PERMATA_ENV=='DEV'?`${host}/apiservice/oauth/token`:`${host}/apiservice/oauth/token`
            var username = process.env.PERMATA_CLIENT_ID;
            var password = process.env.PERMATA_CLIENT_SECRET
            const usernamePasswordBuffer = Buffer.from(username + ':' + password)
            const base64data = usernamePasswordBuffer.toString('base64')
            var data = qs.stringify({
            'grant_type': 'client_credentials' 
            });
            const timestamp = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ')
            const value = `${process.env.PERMATA_API_KEY}:${timestamp}:${data}`
            const signature_raw =  crypto.createHmac("sha256", process.env.PERMATA_CLIENT_STATIC_KEY).update(value).digest()
            const signature = Buffer.from(signature_raw,'binary').toString('base64')
            var config = {
            method: 'post',
            url: url,
            headers: { 
                'Authorization': 'Basic '+base64data,
                'OAUTH-Signature': signature,
                'Content-Type': 'application/x-www-form-urlencoded', 
                'OAUTH-Timestamp': timestamp,
                'API-Key': process.env.PERMATA_API_KEY
            },
            data : data
            };
            console.log(config)
            axios(config).then(resp => {
                    if (resp.isAxiosError==true) {
                        resolve(null)
                    } else 
                    resolve(resp.data.access_token)
                })
        } catch(e){
            console.log(e)
            resolve(null)
        }
        })}
    ,
    getBalance: function (access_token) {
        return new Promise(resolve => {
        var host = process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_DEV_HOST:process.env.PERMATA_HOST
        const timestamp = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ')
        const CustRefID = randtoken.uid(20)
        const payload = {
            "BalInqRq": {
                "MsgRqHdr": {
                    "RequestTimestamp": timestamp,
                    "CustRefID": CustRefID
                },
                "InqInfo": {
                    "AccountNumber": process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_ACCOUNT_NO_DEV:process.env.PERMATA_ACCOUNT_NO_PROD
                }
            }
        }
        console.log(payload)
        const data = JSON.stringify(payload)
        const value = `${access_token}:${timestamp}:${data}`
        const signature_raw =  crypto.createHmac("sha256", process.env.PERMATA_CLIENT_STATIC_KEY).update(value).digest()
        const signature = Buffer.from(signature_raw,'binary').toString('base64')
        var url = process.env.PERMATA_ENV=='DEV'?`${host}/apiservice/InquiryServices/BalanceInfo/inq`:`${host}/apiservice/InquiryServices/BalanceInfo/inq`
            var config = {
            method: 'post',
            url: url,
            headers: { 
                'Content-Type': 'application/json', 
                'authorization': `Bearer ${access_token}`,
                'permata-signature': `${signature}`,
                'organizationname': `${process.env.PERMATA_GROUP_ID}`,
                'permata-timestamp': timestamp,
            },
            data : data
            };
            axios(config).then(resp => {
            if (resp.isAxiosError==true) {
                resolve(null)
            } else {
                console.log(resp.data)
                resolve(resp.data.BalInqRs.InqInfo.AccountBalanceAmount)
                }
            })
    })
    },
    getBIFASTInquiry: function (access_token, account_no, bi_fast_code) {
        return new Promise(resolve => {
        var host = process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_DEV_HOST:process.env.PERMATA_HOST
        const timestamp = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ')
        const CustRefID = Math.floor(Math.pow(10, 16-1) + Math.random() * 9 * Math.pow(10, 16-1)).toString()
        const payload = {
            "beneficiaryAccountNo": `${account_no}`,
            "amount": {
                "value": "0.00",
                "currency": "IDR"
                },
            "purposeOfTransaction": "99", 
            "beneficiaryAccountCode": `${bi_fast_code}`,
            "partnerReferenceNo":`${CustRefID}`
            
    }
        const data = JSON.stringify(payload)
        const value = `${access_token}:${timestamp}:${data}`
        const signature_raw =  crypto.createHmac("sha256", process.env.PERMATA_CLIENT_STATIC_KEY).update(value).digest()
        const signature = Buffer.from(signature_raw,'binary').toString('base64')
        var url = process.env.PERMATA_ENV=='DEV'?`${host}/apiservice/v1/BIFAST/inquiry-Account`:`${host}/apiservice/v1/BIFAST/inquiry-Account`

            var config = {
            method: 'post',
            url: url,
            headers: { 
                'Content-Type': 'application/json', 
                'authorization': `Bearer ${access_token}`,
                'permata-signature': `${signature}`,
                'organizationname': `${process.env.PERMATA_GROUP_ID}`,
                'permata-timestamp': timestamp,
            },
            data : data
            };
            axios(config).then(resp => {
            if (resp.isAxiosError==true) {
                resolve(null)
            } else {
                console.log(resp.data)
                resolve(resp.data)
                }
            })
    })
    }
    ,
    transferMoney(destinationAccountNum, destinationAccountName, destinationBankCode, amount, access_token, partnerReferenceNo){
        return new Promise(resolve => {
        var host = process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_DEV_HOST:process.env.PERMATA_HOST
        var url = process.env.PERMATA_ENV=='DEV'?`${host}/apiservice/v1/BIFAST/credit-Transfer`:`${host}/apiservice/v1/BIFAST/credit-Transfer`
        const timestamp = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ')
        const payload = {
            "partnerReferenceNo": partnerReferenceNo,
            "amount":{"value": amount,"currency":"IDR"},
            "beneficiaryAccountName": destinationAccountName,
            "beneficiaryAccountNo":destinationAccountNum,
            "beneficiaryBankCode":destinationBankCode,
            'customerReference':`BIFAST-CT-${Math.floor(Math.pow(10, 6-1) + Math.random() * 9 * Math.pow(10, 6-1))}`,
            "sourceAccountNo":process.env.PERMATA_ENV=='DEV'?process.env.PERMATA_ACCOUNT_NO_DEV:process.env.PERMATA_ACCOUNT_NO_PROD,
            "transactionDate":timestamp,
        }
        console.log(payload)
         const data = JSON.stringify(payload)
        const value = `${access_token}:${timestamp}:${data}`
        const signature_raw =  crypto.createHmac("sha256", process.env.PERMATA_CLIENT_STATIC_KEY).update(value).digest()
        const signature = Buffer.from(signature_raw,'binary').toString('base64')
        var config = {
        method: 'post',
        url: url,
        headers: { 
            'Content-Type': 'application/json', 
            'authorization': `Bearer ${access_token}`,
            'permata-signature': `${signature}`,
            'organizationname': `${process.env.PERMATA_GROUP_ID}`,
            'permata-timestamp': timestamp,
        },
        data : data
        }
        // axios(config).then(resp => {
        //     console.log(resp.data)
        //     if (resp.isAxiosError==true) {
        //         resolve(null)
        //     } else {
        //         console.log(resp.data)
        //         resolve(resp.data)
        //     } 
        // })
    })
    },
}