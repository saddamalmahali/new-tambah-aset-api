module.exports = {
   apps: [{
      name: "tambahaset-admin-api",
      script: "server.js",
      instances: 1,
      exec_mode: "cluster",
      exp_backoff_restart_delay: 100,
      env: {
         NODE_ENV: "development",
      },
      env_production: {
         NODE_ENV: "production",
      }
   }]
}
