//Cron Controller
//By Fikri akhdi
//Importing library
const coinsRepo = require('../repositories/coins')
const statisticsRepo = require('../repositories/statistics')
const apiHelper = require('../helpers/api')
const moment = require('moment-timezone');
const axios = require('axios')
const cron = require('node-cron')
const jwt = require('jsonwebtoken')
var RateLimiter = require('limiter').RateLimiter;
const fs = require('fs')
const indicators = ['ma', 'ema', 'dema', 'kama', 'macd', 'mama', 'hma'];
const interval = '5m'; // Interval for all coins
const exchange = 'binance'; // Exchange

const secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgwMDI0MjEwLCJleHAiOjMzMTg0NDg4MjEwfQ.eJACarJKO-R6PIxf0rrQ3rrNqylx5Tspy-Tkkk2T5J8'

// const symbols = ['BTC', 'ETH', 'XRP', ...]; // Replace with all 353 coins

const update_coin = async (batch, indicator) => {
   const datas = []
   batch.map((e) => {
      var temp = {
         exchange,
         symbol: `${e}`,
         interval: interval,
         indicators: [],
      }
      var indic = {
         id: e,
         indicator: indicator,
      }
      if (indicator === 'hma') {
         indic.period = 50
      }
      temp.indicators.push(indic)
      datas.push(temp)
   })

   const response = await axios.post("https://api.taapi.io/bulk", {
      secret,
      construct: datas,
   })
   const data = await response.data.data

   // console.log(data)

   var promises_save = []
   data.map(p => {
      promises_save.push(new Promise((resolve, reject) => {
         const jakartaTimezone = 'Asia/Jakarta';
         const now = moment.tz(new Date(), jakartaTimezone);
         var dataCreate = {
            symbol: p.id,
            exchange,
            interval,
            indicator: indicator,
            status: null,
            date_created: now,
         }
         if (indicator == 'macd' || indicator == 'mama') {
            if (indicator == 'macd') {
               dataCreate.value = p.result.valueMACD
            } else if (indicator == 'mama') {
               dataCreate.value = p.result.valueMAMA
            }
         } else {
            dataCreate.value = p.result.value
         }
         var coins = coinsRepo.create(dataCreate)
         resolve(coins)
      }))
   })

   Promise.all(promises_save).then(function (val) {
      return true
   })
   // Do something with the data, such as saving it to a database or processing it further
};
//Function Update Coins
function sleep(ms) {
   return new Promise((resolve) => {
      setTimeout(resolve, ms);
   });
}
async function calculate_coin(interval) {
   try {
      if (interval == '5m') {
         var coins_raw = await coinsRepo.findLatestCoins(49420, '') //353*7*20
      } else if (interval == '60m') {
         var coins_raw = await coinsRepo.findHourlyCoins()
      } else if (interval == '240m') {
         var coins_raw = await coinsRepo.findForthlyCoins()
      }
      var coins = []
      var promises = []
      var max = 20
      var grouped = []
      if (coins_raw.length > 0 && coins_raw != null) {
         console.log('disini 1')
         coins_raw.map(c => {
            if (c.value !== null) {
               pushData(coins, c.symbol, c.value, c.indicator, c.date_created, max)
            }
         })
      }
      coins.map(c => {
         console.log('disini 2')
         var len = c.data.length
         for (var i = len; i >= 0; i--) {
            if (i < len) {
               if (c.data[i] < c.data[i - 1]) {
                  c.nilai.push(1)
               } else {
                  c.nilai.push(0)
               }
            } else {
               c.nilai.push(null)
            }
         }
         var count_1_10 = 0
         var count_0_10 = 0
         var count_1_20 = 0
         var count_0_20 = 0
         var data_10 = null
         var data_20 = null
         data_10 = c.nilai.slice(0, 10)
         data_20 = c.nilai
         data_10.map(d => {
            if (d == 1) {
               count_1_10 += 1
            } else if (d == 0) {
               count_0_10 += 1
            }
         })
         data_20.map(d => {
            if (d == 1) {
               count_1_20 += 1
            } else if (d == 0) {
               count_0_20 += 1
            }
         })
         c.result_10 = count_1_10 > count_0_10 ? "GREEN" : "RED"
         c.result_20 = count_1_20 > count_0_20 ? "GREEN" : "RED"
         c.date = c.date
      })
      coins.map(c => {
         var percentage = 0
         if (symbolExists(grouped, c.symbol)) {
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }
            // if (c.indicator == "ma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "ema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "dema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "kama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "macd") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "mama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "hma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // }
            for (let i = 0; i < grouped.length; i++) {
               if (grouped[i].symbol === c.symbol) {
                  grouped[i].percentage += percentage
               }
            }
         } else {
            var percentage = 0
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }
            // if (c.indicator == "ma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "ema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "dema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "kama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "macd") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "mama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "hma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // }
            //get last price

            //get last date

            var temp = {
               symbol: c.symbol,
               percentage: percentage,
               date: c.date,
               last_price: c.last_price
            }
            grouped.push(temp)
         }
      })
      //sorting
      grouped.sort((a, b) => {
         if (a.percentage < b.percentage) {
            return 1;
         }
         if (a.percentage > b.percentage) {
            return -1;
         }
         return 0;
      });
      var promises = []
      grouped.map(g => {
         promises.push(new Promise((resolve, reject) => {
            const jakartaTimezone = 'Asia/Jakarta';
            const now = moment.tz(new Date(), jakartaTimezone);
            var dataCreate = {
               symbol: g.symbol,
               interval_minute: interval,
               percentage: g.percentage,
               date: now,
               last_price: g.last_price,
            }
            statisticsRepo.create(dataCreate).then(ret => {
               resolve(ret)
            })
         }))
      })

      Promise.all(promises).then(function (val) {
         console.log(val)
         return apiHelper.api_format(true, "Update Data Success")
      })
      // })
   } catch (e) {
      console.log(e)
      return apiHelper.api_format(false, "Something went wrong", 400)
   }
}
async function new_calculate_coin(interval) {
   if (interval == '5m') {
      var coins = await coinsRepo.findLast100M()
      const coinsBySymbol = {};
      const max = 20
      coins.forEach(coin => {
         const symbol = coin.symbol;
         const timestamp = coin.date_created.getTime();
         const timestampRounded = Math.floor(timestamp / (5 * 60 * 1000)) * (5 * 60 * 1000);

         if (!coinsBySymbol[symbol]) {
            coinsBySymbol[symbol] = [];
         }

         const existingCoin = coinsBySymbol[symbol].find(c => c.date_created === timestampRounded);

            if(coinsBySymbol[symbol].length<max){
               if (existingCoin) {
                  if(existingCoin.data.length<max){
                  existingCoin.data = coin.value;
                  }
               } else {
                  coinsBySymbol[symbol].push({
                     date_created: timestampRounded,
                     data: coin.value,
                     indicator: coin.indicator,
                  });
               }
            }
      })
      var final = []
      for(let key in coinsBySymbol) {
         var ob = coinsBySymbol[key]
               var len = ob.length
               for (var i = len; i >= 0; i--) {
                  if (i < len) {
                     console.log(ob[i].data)
                     if(typeof ob[i - 1] !='undefined'){
                     if (ob[i].data < ob[i - 1].data) {
                        pushData(final, key, 1, ob[i].indicator, ob[i].date_created, 20)
                     } else {
                        pushData(final, key, 0, ob[i].indicator, ob[i].date_created, 20)
                     }
                  }
                  } else {
                     // pushData(final, key, null, ob[i].indicator, ob[i].date_created, 20)
                  }
               }
      }
      console.log(final)
      //    ob.map(c=>{
      //       var len = c.data.length
      //       for (var i = len; i >= 0; i--) {
      //          if (i < len) {
      //             if (c.data[i] < c.data[i - 1]) {
      //                c.nilai.push(1)
      //             } else {
      //                c.nilai.push(0)
      //             }
      //          } else {
      //             c.nilai.push(null)
      //          }
      //       }
      //       var count_1_10 = 0
      //       var count_0_10 = 0
      //       var count_1_20 = 0
      //       var count_0_20 = 0
      //       var data_10 = null
      //       var data_20 = null
      //       data_10 = c.nilai.slice(0, 10)
      //       data_20 = c.nilai
      //       data_10.map(d => {
      //          if (d == 1) {
      //             count_1_10 += 1
      //          } else if (d == 0) {
      //             count_0_10 += 1
      //          }
      //       })
      //       data_20.map(d => {
      //          if (d == 1) {
      //             count_1_20 += 1
      //          } else if (d == 0) {
      //             count_0_20 += 1
      //          }
      //       })
      //       c.result_10 = count_1_10 > count_0_10 ? "GREEN" : "RED"
      //       c.result_20 = count_1_20 > count_0_20 ? "GREEN" : "RED"
      //       c.date = c.date
      //    })
      // }
      // console.log(JSON.stringify(coinsBySymbol))
   }
}
// async function update_coin(indicator, interval){
//   try {
//     //get symbols
//     const exchange = 'binance'
//     const secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgwMDI0MjEwLCJleHAiOjMzMTg0NDg4MjEwfQ.eJACarJKO-R6PIxf0rrQ3rrNqylx5Tspy-Tkkk2T5J8'
//     const indicators = [
//       "ma",
//       "ema",
//       "dema",
//       "kama",
//       "macd",
//       "mama",
//       "hma",
//     ]
//     let symbols =  await axios
//     .get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)
//     symbols = symbols.data
//     var PATTERN = '/USDT'
//     symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
//     const datas = []
//       symbols.map((e) => {
//         var temp = {
//           exchange,
//           symbol: `${e}`,
//           interval: '5m',
//           indicators: [],
//         }
//         var indic = {
//           id: e,
//           indicator: indicator,
//         }
//         if(indicator==='hma'){
//           indic.period=50
//         }
//         temp.indicators.push(indic)=
//         datas.push(temp)
//       })
//     const chunkSize = 10
//     let chunks = []
//     const results = []
//     for (let i = 0; i < datas.length; i += chunkSize) {
//         chunks.push(datas.slice(i, i + chunkSize))
//     }
//     //check db
//     //filter
//     //lstone UFT/USDT
//     const last = await coinsRepo.findLatestUpdate(indicator)
//     var number_cut = 0
//     var number_cut_child = 0
//     if(last!=null){
//       if(last.symbol=='OAX/USDT'){
//         number_cut = 0
//       } else {
//         chunks.map((c, index)=>{
//           c.map((d, index_child)=>{
//             if(d.symbol == last.symbol){
//               number_cut = index
//               number_cut_child = index_child
//             }
//           })
//         })
//       }
//     }
//     var max_request = 75
//     var jump = number_cut+max_request
//     chunks = chunks.slice(number_cut,jump) //debug only
//     var promises = []
//     chunks.map(async o=>{
//       promises.push(new Promise((resolve, reject) => {
//           axios
//           .post("https://api.taapi.io/bulk", {
//             secret,
//             construct: o,
//           }).then(async bulk=>{
//             results.push(bulk.data)
//             resolve(bulk.data)
//       })
//     }))
//     })

//     Promise.all(promises).then( function (val) {
//       //simpan ke db
//       var promises_save = []
//       val.map(o=>{
//         o.data.map(p=>{
//           promises_save.push(new Promise((resolve, reject) => {
//           var dataCreate = {
//           symbol: p.id,
//           exchange,
//           interval,
//           indicator:indicator,
//           status: null,
//           date_created: Date.now(),
//           }
//           if(indicator=='macd' ||indicator== 'mama'){
//             if(indicator=='macd'){
//             dataCreate.value = p.result.valueMACD
//             } else if(indicator=='mama'){
//               dataCreate.value = p.result.valueMAMA
//               }
//           } else {
//             dataCreate.value = p.result.value
//           }
//          var coins =  coinsRepo.create(dataCreate)
//          resolve(coins)
//         }))
//       })
//       })

//       Promise.all(promises_save).then( function (val) {
//         return true
//       })
//     })
//   } catch (e) {
//     console.log(e)
//     return false
//   }
// }

exports.calculate_now = async function (req, res) {
   await new_calculate_coin('5m')
   console.log('Running cron new_calculate_coin()');
}
// Function to check if symbol exists in array of objects
function symbolExists(dataArray, symbol, indicator = "") {
   for (let i = 0; i < dataArray.length; i++) {
      if (indicator != "") {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            return true;
         }
      } else {
         if (dataArray[i].symbol === symbol) {
            return true;
         }
      }
   }
   return false;
}

// Function to push data to existing symbol or create new object
function pushData(dataArray, symbol, data, indicator, date, max) {
   if (symbolExists(dataArray, symbol, indicator)) {
      for (let i = 0; i < dataArray.length; i++) {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            if (dataArray[i].date < date) {
               dataArray[i].date = date
            }
            if (dataArray[i].indicator === 'ma' && dataArray[i].date < date) {
               dataArray[i].last_price = data
            }
            if (dataArray[i].data.length < max) {
               dataArray[i].data.push(data);
               break;
            }
         }
      }
   } else {
      dataArray.push({ symbol: symbol, data: [data], nilai: [], indicator: indicator, date: date, last_price: data });
   }
}

// cron.schedule('*/5 * * * *', async function () {
//    let res = await calculate_coin('5m')
//    console.log('Running cron calculate_coin(5m) :', res);
// })
// cron.schedule('0 * * * *', async function () {
//    let res = await calculate_coin('60m')
//    console.log('Running cron calculate_coin(60m) :', res);
// })
// cron.schedule('0 */4 * * *', async function () {
//    let res = await calculate_coin('240m');
//    console.log('Running cron calculate_coin(240m) :', res);
// })
// cron.schedule('0 */81 * * *', () => {
//    coinsRepo.deleteCoins()
//    console.log('Running cron deleteCoins');
//    statisticsRepo.deleteStatistics()
//    console.log('Running cron deleteStatistics');
// });
// cron.schedule('* * * * *', () => {
//    console.log('Test running cron every minute');
// });

// cron.schedule('* * * * *', async () => {
//    console.log('Running cron update_coin');
//    let symbols = await axios.get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)
//    symbols = symbols.data
//    var PATTERN = '/USDT'
//    symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
//    const chunkSize = 10
//    let chunks = []
//    for (let i = 0; i < symbols.length; i += chunkSize) {
//       chunks.push(symbols.slice(i, i + chunkSize))
//    }
//    for await (const indicator of indicators) {
//       for await (const chunk of chunks) {
//          await update_coin(chunk, indicator);
//          // Wait for 2 milisecond before making the next batch of requests
//          // Total 75 Request per 15 Seconds
//          await new Promise((resolve) => setTimeout(resolve, 200));
//       }
//    }
// });
