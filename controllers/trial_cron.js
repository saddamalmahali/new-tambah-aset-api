//Cron Controller
//By Fikri akhdi
//Importing library
const coinsRepo = require('../repositories/coins')
const apiHelper = require('../helpers/api')
const axios = require('axios')
const moment = require('moment')
const jwt = require('jsonwebtoken')
var RateLimiter = require('limiter').RateLimiter;
const fs = require('fs')

//Function Update Coins
function wait(ms) {
  return new Promise((resolve) => { setTimeout(resolve, ms) });
}
exports.update_coins = async function (req, res) {
  res.type('json')
  try {
    //get symbols
    const exchange = 'binance'
    const secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgwMDI0MjEwLCJleHAiOjMzMTg0NDg4MjEwfQ.eJACarJKO-R6PIxf0rrQ3rrNqylx5Tspy-Tkkk2T5J8'
    const interval = '5m'
    const indicators = [
      "ma",
      "ema",
      "dema",
      "kama",
      "macd",
      "mama",
      "hma",
    ]
    let symbols = await axios
      .get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)
    symbols = symbols.data
    var PATTERN = '/USDT'
    symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
    const data_ma = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "ma",
          },
        ],
      })
    })
    const data_ema = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "ema",
          },
        ],
      })
    })
    const data_dema = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "dema",
          },
        ],
      })
    })
    const data_kama = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "kama",
          },
        ],
      })
    })
    const data_macd = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "macd",
          },
        ],
      })
    })
    const data_mama = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "mama",
          },
        ],
      })
    })
    const data_hma = []
    symbols.map((e) => {
      datas.push({
        exchange,
        symbol: `${e}`,
        interval,
        indicators: [
          {
            id: e,
            indicator: "hma",
          },
        ],
      })
    })
    const chunkSize = 10
    let chunks_ma = []
    let chunks_ema = []
    let chunks_dema = []
    let chunks_kama = []
    let chunks_macd = []
    let chunks_mama = []
    let chunks_hma = []
    const results = []

    for (let i = 0; i < data_ma.length; i += chunkSize) {
      chunks_ma.push(data_ma.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_ema.length; i += chunkSize) {
      chunks_ema.push(data_ema.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_dema.length; i += chunkSize) {
      chunks_dema.push(data_dema.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_kama.length; i += chunkSize) {
      chunks_kama.push(data_kama.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_macd.length; i += chunkSize) {
      chunks_macd.push(data_macd.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_mama.length; i += chunkSize) {
      chunks_mama.push(data_mama.slice(i, i + chunkSize))
    }
    for (let i = 0; i < data_hma.length; i += chunkSize) {
      chunks_hma.push(data_hma.slice(i, i + chunkSize))
    }
    //check db
    //filter
    //lstone OAX/USDT

    const last_ma = await coinsRepo.findLatestUpdate("ma")
    const last_ema = await coinsRepo.findLatestUpdate("ema")
    const last_dema = await coinsRepo.findLatestUpdate("dema")
    const last_kama = await coinsRepo.findLatestUpdate("kama")
    const last_macd = await coinsRepo.findLatestUpdate("macd")
    const last_mama = await coinsRepo.findLatestUpdate("mama")
    const last_hma = await coinsRepo.findLatestUpdate("hma")
    var number_cut_ma = 0
    var number_cut_child_ma = 0
    var number_cut_ema = 0
    var number_cut_child_ema = 0
    var number_cut_dema = 0
    var number_cut_child_dema = 0
    var number_cut_kama = 0
    var number_cut_child_kama = 0
    var number_cut_macd = 0
    var number_cut_child_macd = 0
    var number_cut_mama = 0
    var number_cut_child_mama = 0
    var number_cut_hma = 0
    var number_cut_child_hma = 0
    if (last_ma != null) {
      if (last_ma.symbol == 'OAX/USDT') {
        number_cut_ma = 0
      } else {
        chunks_ma.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_ma.symbol) {
              number_cut_ma = index
              number_cut_child_ma = index_child
            }
          })
        })
      }
    }
    if (last_ema != null) {
      if (last_ema.symbol == 'OAX/USDT') {
        number_cut_ema = 0
      } else {
        chunks_ema.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_ema.symbol) {
              number_cut_ema = index
              number_cut_child_ema = index_child
            }
          })
        })
      }
    }
    if (last_dema != null) {
      if (last_dema.symbol == 'OAX/USDT') {
        number_cut_dema = 0
      } else {
        chunks_dema.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_dema.symbol) {
              number_cut_dema = index
              number_cut_child_dema = index_child
            }
          })
        })
      }
    }
    if (last_kama != null) {
      if (last_kama.symbol == 'OAX/USDT') {
        number_cut_kama = 0
      } else {
        chunks_kama.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_kama.symbol) {
              number_cut_kama = index
              number_cut_child_kama = index_child
            }
          })
        })
      }
    }
    if (last_macd != null) {
      if (last_macd.symbol == 'OAX/USDT') {
        number_cut_macd = 0
      } else {
        chunks_macd.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_macd.symbol) {
              number_cut_macd = index
              number_cut_child_macd = index_child
            }
          })
        })
      }
    }
    if (last_mama != null) {
      if (last_mama.symbol == 'OAX/USDT') {
        number_cut_macd = 0
      } else {
        chunks_mama.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_mama.symbol) {
              number_cut_mama = index
              number_cut_child_mama = index_child
            }
          })
        })
      }
    }
    if (last_hma != null) {
      if (last_hma.symbol == 'OAX/USDT') {
        number_cut_hma = 0
      } else {
        chunks_hma.map((c, index) => {
          c.map((d, index_child) => {
            if (d.symbol == last_hma.symbol) {
              number_cut_hma = index
              number_cut_child_hma = index_child
            }
          })
        })
      }
    }
    "ma",
      "ema",
      "dema",
      "kama",
      "macd",
      "mama",
      "hma",
    var max_request = 75
    var jump_ma = number_cut_ma + max_request
    chunks_ma = chunks_ma.slice(number_cut_ma, jump_ma)
    var jump_ema = number_cut_ema + max_request
    chunks_ema = chunks_ma.slice(number_cutema, jump_ema)
    var promises = []
    chunks_ma.map(async o => {
      promises.push(new Promise((resolve, reject) => {
        axios
          .post("https://api.taapi.io/bulk", {
            secret,
            construct: o,
          }).then(async bulk => {
            resolve(bulk.data)
          })
      }))
    })

    Promise.all(promises).then(function (val) {
      //simpan ke db
      var promises_save = []
      val.map(o => {
        console.log(JSON.stringify(o))
        o.data.map(p => {
          promises_save.push(new Promise((resolve, reject) => {
            var dataCreate = {
              symbol: p.id,
              exchange,
              value: p.result.value,
              interval,
              total_nominal: p.result.value,
              status: null,
              date_created: Date.now(),
            }
            var coins = coinsRepo.create(dataCreate)
            resolve(coins)
          }))
        })
      })

      Promise.all(promises_save).then(function (val) {
        res.type('json').send(apiHelper.api_format(true, "Update Coins Success"))
      })
    })
    // const max = 1
    // const results = []
    // const backtracked = []
    // for (let i = 0; i < max; i += 1) {
    //   var bulk = await axios
    //   .post("https://api.taapi.io/bulk", {
    //     secret,
    //     construct: chunks[i],
    //   })
    //   results.push(bulk.data)
    // }
    // results.map(async o=>{
    //   o.data.map(async p=>{await axios
    //     .post("https://api.taapi.io/bulk", {
    //       secret,
    //       construct: {
    //         exchange: "binance",
    //         symbol: `${p.id}`,
    //         interval: "5m",
    //         indicators: [
    //           {
    //             // MACD Backtracked 1
    //             id: `${p.id}`,
    //             indicator: "ma",
    //             backtracks: 10,
    //           },
    //         ],
    //       },
    //     }).then(resu=>{
    //       console.log(resu)
    //     })
    //   })
    // })
    // axios
    //     .post("https://api.taapi.io/bulk", {
    //       secret,
    //       construct: {
    //         exchange: "binance",
    //         symbol: `ONT/BTC`,
    //         interval: "5m",
    //         indicators: [
    //           {
    //             // MACD Backtracked 1
    //             id: `ONT/BTC`,
    //             indicator: "ma",
    //             backtracks: 10,
    //           },
    //         ],
    //       },
    //     }).then(result=>{
    //       console.log(JSON.stringify(result.data, null, 4))
    //     })
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

exports.dashboard = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
    var where = {

    }
    var coins_raw = await coinsRepo.findByWhere(where)
    var coins = []
    var promises = []
    var max = 10
    if (coins_raw.length > 0) {
      coins_raw.map(c => {
        if (c.value !== null) {
          pushData(coins, c.symbol, c.value, max)
        }
      })
    }
    coins.map(c => {
      for (var i = 1; i <= max; i++) {
        if (c.data[i] > c.data[i - 1]) {
          c.nilai.push(1)
        } else {
          c.nilai.push(0)
        }
      }
    })
    res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", coins))
    // })
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

// Function to check if symbol exists in array of objects
function symbolExists(dataArray, symbol) {
  for (let i = 0; i < dataArray.length; i++) {
    if (dataArray[i].symbol === symbol) {
      return true;
    }
  }
  return false;
}

// Function to push data to existing symbol or create new object
function pushData(dataArray, symbol, data, max) {
  if (symbolExists(dataArray, symbol)) {
    for (let i = 0; i < dataArray.length; i++) {
      if (dataArray[i].symbol === symbol) {
        if (dataArray[i].data.length < max) {
          dataArray[i].data.push(data);
          break;
        }
      }
    }
  } else {
    dataArray.push({ symbol: symbol, data: [data], status: 'red', nilai: [] });
  }
}