//Authentication Controller
//By Fikri akhdi
//Importing library
const axios = require('axios')
const usersRepo = require('../repositories/users')
const apiHelper = require('../helpers/api')
const useragent = require('express-useragent')

const crypto = require('crypto')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const {
  Validator
} = require('node-input-validator')

//Loading Key
var publicKEY = fs.readFileSync('./key/public.key', 'utf8')
var privateKEY = fs.readFileSync('./key/private.key', 'utf8')
//Set JWT Options
//Token Function
exports.token = async function (req, res) {
  res.type('json')
  //Fetching Data
  var source = req.headers['user-agent'],
    remoteDevice = useragent.parse(source)
  var username = req.body.username.toLowerCase()
  var password = req.body.password
  const validation = new Validator(req.body, {
    username: 'required|email',
    password: 'required',
  })

  try {
    //Checking Required Fields
    var matched = await validation.check()
    if (matched) {
      var ipAddr = req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null)
        ipAddr = ipAddr.split(",")[0]
        var where = {
          email: username,
          password: password,
          is_deleted: null,
        }
        var user = await usersRepo.findByWhere(where)
        if (user) {
          if(user.length>0){
          var user = user[0]
          if(user.is_active==1){
          var lookupURL = process.env.IP_LOOKUP_URL
          var lookupApiKey = process.env.IP_LOOKUP_ACCESS_KEY
          //get ip lookup by API
          var ip_lookup = await axios.get(lookupURL + ipAddr + "?access_key=" + lookupApiKey)
          if (ip_lookup.data) {
            var country = ip_lookup.data.country_name ? ip_lookup.data.country_name : "unknown"
            var city = ip_lookup.data.region_name ? ip_lookup.data.region_name : "unknown"
          } else {
            var country = "unknown"
            var city = "unknown"
          }
          //create login log
          var dataCreate = {
            type: 'login',
            title: 'User Login',
            description: username+' has logged in',
            device_information: remoteDevice.os+ ' ' +remoteDevice.browser+ ' '+remoteDevice.platform+ ' ' +country+' '+city,
            ip_address: ipAddr,
            user_id: user.id,
            date_created: Date.now(),
            created_by: user.id
          }
          if(user.company) dataCreate.company =  user.company.id
          // if (loginLog) {
            // var loginId = loginLog.login_log_id
              let payload = {
                id: user.id,
              }
              let jwtOptions = {
                expiresIn: req.body.remember_me==1?'10000d':process.env.JWT_EXPIRES+'h',
                algorithm: process.env.JWT_ALGORITHM
              }
              let token = jwt.sign(payload, privateKEY, jwtOptions)
              let refreshToken = jwt.sign(payload, privateKEY, jwtOptions)
              var data_ret = {
                accessToken: {
                  userData: user,
                  token: token,
                  expiresIn: req.body.remember_me==1?9999999:process.env.JWT_EXPIRES+'h',
                  algorithm: jwtOptions.algorithm,
                },
                refreshToken: refreshToken,
              }
              // io.emit('deals', { message: 'Update Deal', data: "HAHAHA" })
              // console.log("Emitting "+'newmail_' + user.user_gmail_email)
            //   io.emit('newmail_' + user.user_gmail_email, {
            //     message: 'New Mail',
            //     data: "Empty"
            // })
              res.type('json').end(apiHelper.api_format(true, "Authorization success", "", data_ret))
          // } else {
          //   res.type('json').end(apiHelper.api_format(false, "Get data failed", 400))
          // }
            } else {
              res.type('json').end(apiHelper.api_format(false, 'Your account is not active yet', 400))
            }
        } else {
          res.type('json').end(apiHelper.api_format(false, 'Username or Password is wrong', 400))
        }
        } else {
          // Consume 1 point from limiters on wrong attempt and block if limits reached
          try {
            res.type('json').status(429)
            res.type('json').end(apiHelper.api_format(false, 'Username or Password is wrong', 400))
          } catch (rlRejected) {
            if (rlRejected instanceof Error) {
              throw rlRejected
            } else {
              res.set('Retry-After', String(Math.round(rlRejected.msBeforeNext / 1000)) || 1)
              res.type('json').end(apiHelper.api_format(false, 'Too Many Requests', 429))
            }
          }
        }
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}
//Refresh Token
exports.refresh_token = function (req, res) {
  res.type('json')
  try {
        var userId = res.user_id
        let payload = {
          id: userId,
        }
        console.log(payload)
        let token = jwt.sign(payload, privateKEY, jwtOptions)

        //create for the next 
        let Options = {
          expiresIn: (60 * process.env.JWT_EXPIRES) * 2,
          algorithm: process.env.JWT_ALGORITHM
        }
        let refreshToken = jwt.sign(payload, privateKEY, Options)
        var data_ret = {
          accessToken: {
            token: token,
            expiresIn: process.env.JWT_EXPIRES,
            algorithm: jwtOptions.algorithm
          },
          refreshToken: refreshToken,
        }
        res.type('json').end(apiHelper.api_format(true, "Refresh token success", null, data_ret))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}


//Function logout
exports.logout = async function (req, res) {
  res.type('json')
  //Fetching Data
  if (req.header('authorization')) {
    var ipAddr = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null)
      ipAddr = ipAddr.split(",")[0]
    var source = req.headers['user-agent'],
      remoteDevice = useragent.parse(source)
    try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
          var lookupURL = process.env.IP_LOOKUP_URL
          var lookupApiKey = process.env.IP_LOOKUP_ACCESS_KEY
          //get ip lookup by API
          var ip_lookup = await axios.get(lookupURL + ipAddr + "?access_key=" + lookupApiKey)
          if (ip_lookup.data) {
            var country = ip_lookup.data.country_name ? ip_lookup.data.country_name : "unknown"
            var city = ip_lookup.data.region_name ? ip_lookup.data.region_name : "unknown"
          } else {
            var country = "unknown"
            var city = "unknown"
          }
          //create login log
          var dataCreate = {
            type: 'logout',
            title: 'User Logout',
            description: username+' has logged out',
            device_information: remoteDevice.os+ ' ' +remoteDevice.browser+ ' '+remoteDevice.platform+ ' ' +country+' '+city,
            ip_address: ipAddr,
            user_id: res.user_id,
            company_id: user.company.id,
            date_created: Date.now(),
            created_by: res.user_id
          }
          res.type('json').end(apiHelper.api_format(true, "Logout success", ""))
        } else {
          res.type('json').end(apiHelper.api_format(false, "Invalid User", 401))
        }
    } catch (e) {
      res.type('json').end(apiHelper.api_format(false, "Something went wrong", 400))
    }
  } else {
    res.type('json').end(apiHelper.api_format(false, "Token Required", 400))
  }
}