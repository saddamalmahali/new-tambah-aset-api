//Coins Controller
//By Fikri akhdi
//Importing library
const coinsRepo = require('../repositories/coins')
const statisticsRepo = require('../repositories/statistics')
const statisticModel = require('../models/statistics')
const apiHelper = require('../helpers/api')
const {
   Op
} = require('sequelize')
const axios = require('axios')
const cron = require('node-cron')
const moment = require('moment')
const jwt = require('jsonwebtoken')
var RateLimiter = require('limiter').RateLimiter;
const fs = require('fs')

const secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgwMDI0MjEwLCJleHAiOjMzMTg0NDg4MjEwfQ.eJACarJKO-R6PIxf0rrQ3rrNqylx5Tspy-Tkkk2T5J8'
//Function Update Coins

exports.dashboard = async function (req, res) {
   res.type('json')
   var dateNow = new Date()
   try {
      var limit = {
         limit: 353
      }
      var search = req.body.search
      var interval = req.body.interval

      var where = {
         interval_minute: interval
      }

      if (search != "" && typeof search != 'undefined') {
         where.symbol = {
            $regex: '.*' + search + '.*'
         }
      }


      var statistics_raw = await statisticsRepo.findByWhere(where, limit)

      var statistics = []

      if (statistics_raw != null) {
         statistics_raw.sort((a, b) => {
            if (a.percentage < b.percentage) {
               return 1;
            }
            if (a.percentage > b.percentage) {
               return -1;
            }
            return 0;
         })
         statistics_raw.map(o => {
            if (!symbolExists(statistics, o.symbol)) {
               statistics.push(o)
            }
         })
      }

      statistics = statistics.slice(0, 10)

      var new_price = []

      statistics.map(async o => {
         new_price.push(new Promise((resolve, reject) => {
            var price = 0
            coinsRepo.findLatestPrice(o.symbol).then(ret => {
               price = ret.value
               var temp = {
                  symbol: o.symbol,
                  interval_minute: '5m',
                  percentage: o.percentage,
                  date: new Date(),
                  last_price: price
               }
               resolve(temp)
            })
         }))
      })

      Promise.all(new_price).then(function (val) {
         // order by percentage
         // for (let i = val.length - 1; i > 0; i--) {
         //   const j = Math.floor(Math.random() * (i + 1));
         //   [val[i], val[j]] = [val[j], val[i]];
         // }
         val.sort((a, b) => b.percentage - a.percentage)
         res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", val))
      })

      // })
   } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, e.message, 400))
   }
}

exports.getSuggestion = async function (req, res) {
   res.type('json')
   try {
      var search = req.body.search
      var where = {}
      if (search != "" && typeof search != 'undefined') {
         where.symbol = {
            $regex: '.*' + search + '.*'
         }
      }
      var suggestion_raw = await coinsRepo.getSuggestion(where)
      var suggestions = []
      if (suggestion_raw != null) {
         suggestion_raw.map(o => {
            suggestions.push(o.symbol)
         })
      }
      res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", suggestions))

      // })
   } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, e.message, 400))
   }
}

exports.countSpecific = async function (req, res) {
   try {
      var coins_raw = await coinsRepo.findSpecificCoin(req.body.symbol, req.body.interval) //353*7*20
      var interval = req.body.interval
      var coins = []
      var promises = []
      var max = 20
      var grouped = []
      if (coins_raw.length > 0 && coins_raw != null) {
         coins_raw.map(c => {
            if (c.value !== null) {
               pushData(coins, c.symbol, c.value, c.indicator, c.date_created, max)
            }
         })
      }
      coins.map(c => {
         var len = c.data.length
         for (var i = len; i >= 0; i--) {
            if (i < len) {
               if (i == 0) {
                  if (c.data[0] > c.data[1]) {
                     c.nilai.push(1)
                  } else {
                     c.nilai.push(0)
                  }
               } else {
                  if (c.data[i] < c.data[i - 1]) {
                     c.nilai.push(1)
                  } else {
                     c.nilai.push(0)
                  }
               }
            }
         }
         var count_1_10 = 0
         var count_0_10 = 0
         var count_1_20 = 0
         var count_0_20 = 0
         var data_10 = null
         var data_20 = null
         data_10 = c.nilai.slice(0, 10)
         data_20 = c.nilai
         data_10.map(d => {
            if (d == 1) {
               count_1_10 += 1
            } else if (d == 0) {
               count_0_10 += 1
            }
         })
         data_20.map(d => {
            if (d == 1) {
               count_1_20 += 1
            } else if (d == 0) {
               count_0_20 += 1
            }
         })
         c.result_10 = "RED"
         c.result_20 = "RED"
         if (data_10[0] == 1 && count_1_10 > 5) {
            c.result_10 = "GREEN"
         }
         if (data_20[0] == 1 && count_1_20 > 5) {
            c.result_20 = "GREEN"
         }
         c.date = c.date
      })
      coins.map(c => {
         var percentage = 0
         if (symbolExists(grouped, c.symbol)) {
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }
            for (let i = 0; i < grouped.length; i++) {
               if (grouped[i].symbol === c.symbol) {
                  grouped[i].percentage += percentage
               }
            }
         } else {
            var percentage = 0
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }
            // if (c.indicator == "ma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "ema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "dema") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "kama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "macd") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "mama") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // } else if (c.indicator == "hma") {
            //    percentage = c.result_10 == 'GREEN' && c.result_20 == 'GREEN' ? 5 : 0
            // }
            //get last price

            //get last date

            var temp = {
               symbol: c.symbol,
               percentage: percentage,
               date: c.date,
               last_price: c.last_price
            }
            grouped.push(temp)
         }
      })
      //sorting
      grouped.sort((a, b) => {
         if (a.percentage < b.percentage) {
            return 1;
         }
         if (a.percentage > b.percentage) {
            return -1;
         }
         return 0;
      });
      var promises = []

      res.type('json').send(apiHelper.api_format(true, 'Success', '', coins))
   } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, e.message, 400))
   }
}

// Function to check if symbol exists in array of objects
function symbolExists(dataArray, symbol, indicator = "") {
   for (let i = 0; i < dataArray.length; i++) {
      if (indicator != "") {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            return true;
         }
      } else {
         if (dataArray[i].symbol === symbol) {
            return true;
         }
      }
   }
   return false;
}

// Function to push data to existing symbol or create new object
function pushData(dataArray, symbol, data, indicator, date, max) {
   if (symbolExists(dataArray, symbol, indicator)) {
      for (let i = 0; i < dataArray.length; i++) {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            if (dataArray[i].data.length < max) {
               dataArray[i].data.push(data);
               break;
            }
         }
      }
   } else {
      dataArray.push({ symbol: symbol, data: [data], nilai: [], indicator: indicator, date: date });
   }
}