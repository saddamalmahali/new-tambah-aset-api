//Common Controller
//By Fikri akhdi
//Importing library
const usersRepo = require('../repositories/users')
const banksRepo = require('../repositories/banks')
const businessSectionsRepo = require('../repositories/business_sections')
const citiesRepo = require('../repositories/cities')
const districtsRepo = require('../repositories/districts')
const provincesRepo = require('../repositories/provinces')
const subdistrictsRepo = require('../repositories/subdistricts')
const rolesRepo = require('../repositories/roles')
const apiHelper = require('../helpers/api')
const randtoken = require('rand-token');
const crypto = require('crypto')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const fs = require('fs')

//Function Bank List
exports.banks = async function (req, res) {
  res.type('json')
  try {
        var banks = await banksRepo.list()
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", banks))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Business Sections List
exports.business_sections = async function (req, res) {
  res.type('json')
  try {
        var list = await businessSectionsRepo.list()
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", list))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Provinces List
exports.provinces = async function (req, res) {
  res.type('json')
  try {
        var list = await provincesRepo.list()
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", list))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Cities List
exports.cities = async function (req, res) {
  res.type('json')
  try {
        var list = await citiesRepo.findByProvinceId(req.params.id)
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", list))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong ", 400))
  }
}

//Function Districts List
exports.districts = async function (req, res) {
  res.type('json')
  try {
    var list = await districtsRepo.findByCityId(req.params.id)
    res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", list))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Subdistricts List
exports.subdistricts = async function (req, res) {
  res.type('json')
  try {
    var list = await subdistrictsRepo.findByDistrictId(req.params.id)
    res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", list))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Roles List
exports.roles = async function (req, res) {
    res.type('json')
    try {
      var roles = await rolesRepo.list()
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", roles))
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
  }


//Function verify
exports.verify = async function (req, res) {
  //Fetching Data
    var dateNow = new Date()
    try {
      if(req.params.id){
        var code = Buffer.from(req.params.id, 'base64').toString('ascii')
        var id = code.split(":")[0]
        var email = code.split(":")[1]
        const where = {
          id: id,
          email: email,
          is_active:0,
        }
        var matched = await usersRepo.findByWhere(where)
          if (matched.length>0) {
            var dataUpdate = {
              is_active: true,
              date_modified: dateNow,
              modified_by: matched[0].id
            }
            var updated = await usersRepo.updateData(matched[0].id, dataUpdate)
            if (updated){
            res.type('json').end(apiHelper.api_format(true, "Verification Success"))
            }
            else
            res.type('json').end(apiHelper.api_format(false, "Verification Failed!", 400))
          } else {
            res.type('json').end(apiHelper.api_format(false, "Invalid Verification Code!", 400))
          }
        } else {
          res.type('json').end(apiHelper.api_format(false, "Verification Code is required!", 400))
        }
    } catch (e) {
      console.log(e)
      res.type('json').status(401)
      res.type('json').end(apiHelper.api_format(false, "Invalid Verification Code!", 400))
    }
}
  
//Function reset password
exports.reset_password = async function (req, res) {
  //Fetching Data
    var dateNow = new Date()
    try {
            var where = {
              email : req.body.email
            }
            var user = await usersRepo.findByWhere(where)
            if(user.length>0){
            var new_password = randtoken.uid(16).substr(0,5)
            var dataUpdate = {
              password: crypto.createHash('sha256').update(new_password).digest('base64'),
              date_modified: dateNow,
              modified_by: user[0].id
            }
            var updated = await usersRepo.updateData(user[0].id, dataUpdate)
            if (updated){
              //send mail
                var jobData = {
                  new_password: new_password,
                  email_to: req.body.email,
                  year: new Date().getFullYear(),
              }

              const job = resetQueue.createJob(jobData);
              job.save();
              res.type('json').end(apiHelper.api_format(false, "Reset Password Success", 400))
            }
            else
            res.type('json').end(apiHelper.api_format(false, "Something went wrong", 400))
          } else {
            res.type('json').end(apiHelper.api_format(false, "Email is not registered", 400))
          }
    } catch (e) {
      console.log(e)
      res.type('json').status(401)
      res.type('json').end(apiHelper.api_format(false, "Something went wrong", 401))
    }
}