//Cron Controller
//By Fikri akhdi
//Importing library
const coinsRepo = require('../repositories/coins')
const statisticsRepo = require('../repositories/statistics')
const apiHelper = require('../helpers/api')
const moment = require('moment-timezone');
const axios = require('axios')
const cron = require('node-cron')
const jwt = require('jsonwebtoken')
var RateLimiter = require('limiter').RateLimiter;
const fs = require('fs')
const indicators = ['ma', 'ema', 'dema', 'kama', 'macd', 'mama', 'hma'];
const exchange = 'binance'; // Exchange

const secret = process.env.TAAPI_SECRET

// const symbols = ['BTC', 'ETH', 'XRP', ...]; // Replace with all 353 coins
// ---------------------------------------
const update_coin = async (batch, indicator, interval) => {
   try {
      const datas = []
      batch.map((e) => {
         var temp = {
            exchange,
            symbol: `${e}`,
            interval: '5m',
            indicators: [],
         }

         var indic = {
            id: e,
            indicator: indicator,
         }

         if (indicator === 'hma') {
            indic.period = 50
         }

         temp.indicators.push(indic)
         datas.push(temp)
      })

      const response = await axios.post("https://api.taapi.io/bulk", {
         secret,
         construct: datas,
      })

      const data = await response.data.data

      // console.log(data)
      const now = moment().format("YYYY-MM-DD hh:mm:ss");
      var promises_save = []
      data.map(p => {
         promises_save.push(new Promise((resolve, reject) => {
            const jakartaTimezone = 'Asia/Jakarta';

            coinsRepo.findPreviousData(p.id, interval).then(prev => {

               var dataCreate = {
                  symbol: p.id,
                  exchange,
                  interval,
                  indicator: indicator,
                  date_created: now,
               }


               let value_data = 0;
               if (indicator == 'macd' || indicator == 'mama') {
                  if (indicator == 'macd') {
                     // dataCreate.value = p.result.valueMACD
                     value_data = p.result.valueMACD || 0
                  } else if (indicator == 'mama') {
                     // dataCreate.value = p.result.valueMAMA
                     value_data = p.result.valueMAMA || 0
                  }
               } else {
                  value_data = p.result.value || 0
               }

               dataCreate.value = value_data

               if (prev != null) {
                  if (dataCreate.value > prev.value) {
                     dataCreate.status = 1
                  } else {
                     dataCreate.status = 0
                  }
               } else {
                  dataCreate.status = 0;
               }
               coinsRepo.create(dataCreate).then(coins => {
                  resolve(coins)
               })
            })
         }))
      })


      Promise.all(promises_save).then(function (val) {
         return true
      })
   } catch (err) {
      console.log(err)
      return false
   }
};

//Function Update Coins
function sleep(ms) {
   return new Promise((resolve) => {
      setTimeout(resolve, ms);
   });
}

async function calculate_coin(interval) {
   try {
      var coins_raw = await coinsRepo.findLatestCoins(49420) //353*7*20
      var coins = []
      var promises = []
      var max = 20
      var grouped = []
      if (coins_raw.length > 0 && coins_raw != null) {
         coins_raw.map(c => {
            if (c.value !== null) {
               pushData(coins, c.symbol, c.value, c.indicator, c.date_created, c.status, max)
            }
         })
      }
      coins.map(c => {
         var len = c.data.length
         // for (var i = len; i >= 0; i--) {
         //    if (i < len) {
         //       if(i==0){
         //         if(c.data[0]>c.data[1]){
         //           c.nilai.push(1)
         //          } else {
         //            c.nilai.push(0)
         //          }
         //       } else {
         //        if (c.data[i] < c.data[i - 1]) {
         //           c.nilai.push(1)
         //        } else {
         //           c.nilai.push(0)
         //        }
         //       }
         //     }
         // }
         var count_1_10 = 0
         var count_0_10 = 0
         var count_1_20 = 0
         var count_0_20 = 0
         var data_10 = null
         var data_20 = null
         data_10 = c.status.slice(0, 10)
         data_20 = c.status
         data_10.map(d => {
            if (d == 1) {
               count_1_10 += 1
            } else if (d == 0) {
               count_0_10 += 1
            }
         })
         data_20.map(d => {
            if (d == 1) {
               count_1_20 += 1
            } else if (d == 0) {
               count_0_20 += 1
            }
         })
         c.result_10 = "RED"
         c.result_20 = "RED"
         if (data_10[0] == 1 && count_1_10 > 5) {
            c.result_10 = "GREEN"
         }
         if (data_20[0] == 1 && count_1_20 > 5) {
            c.result_20 = "GREEN"
         }
         c.date = c.date
      })
      coins.map(c => {
         var percentage = 0
         if (symbolExists(grouped, c.symbol)) {
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }
            for (let i = 0; i < grouped.length; i++) {
               if (grouped[i].symbol === c.symbol) {
                  grouped[i].percentage += percentage
               }
            }
         } else {
            var percentage = 0
            if (c.indicator == "ma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 6 : 0
            } else if (c.indicator == "ema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 7 : 0
            } else if (c.indicator == "dema") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 8 : 0
            } else if (c.indicator == "kama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 9 : 0
            } else if (c.indicator == "macd") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 10 : 0
            } else if (c.indicator == "mama") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 11 : 0
            } else if (c.indicator == "hma") {
               percentage = c.result_10 == 'GREEN' ? 5 : 0
               percentage += c.result_20 == 'GREEN' ? 12 : 0
            }

            //get last date

            var temp = {
               symbol: c.symbol,
               percentage: percentage,
               date: c.date,
               last_price: c.last_price
            }
            grouped.push(temp)
         }
      })

      //sorting
      grouped.sort((a, b) => {
         if (a.percentage < b.percentage) {
            return 1;
         }
         if (a.percentage > b.percentage) {
            return -1;
         }
         return 0;
      });

      var promises = []
      grouped.map(g => {
         promises.push(new Promise((resolve, reject) => {
            const jakartaTimezone = 'Asia/Jakarta';
            const now = moment.tz(new Date(), jakartaTimezone);
            var dataCreate = {
               symbol: g.symbol,
               interval_minute: interval,
               percentage: g.percentage,
               date: now,
               last_price: g.last_price,
            }
            statisticsRepo.create(dataCreate).then(ret => {
               resolve(ret)
            })
         }))
      })

      Promise.all(promises).then(function (val) {
         console.log(val)
         return apiHelper.api_format(true, "Update Data Success")
      })
      // })
   } catch (e) {
      console.log(e)
      return apiHelper.api_format(false, "Something went wrong", 400)
   }
}

const manual_fetch = async function (req, res) {
   res.type('json')

   let interval = req.body.interval || "15m"

   console.log('Running cron update_coin ' + interval);
   let symbols = await axios.get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)

   symbols = symbols.data


   var PATTERN = '/USDT'
   symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
   const chunkSize = 10
   let chunks = []
   for (let i = 0; i < 100; i += chunkSize) {
      chunks.push(symbols.slice(i, i + chunkSize))
   }

   // res.send({ status: 200, message: 'Berhasil Get Data Coin', data: req.body.interval })
   for await (const indicator of indicators) {
      for await (const chunk of chunks) {
         await update_coin(chunk, indicator, interval);
         await new Promise((resolve) => setTimeout(resolve, 200));
      }
   }
   let dataResponse = await calculate_coin('5m')
   res.type('json').end(dataResponse)
   // res.send({ status: 200, message: 'Hit Coin Done!' })
}

const manual_fetch_function = async function (interval) {

   console.log('Running cron update_coin ' + interval);
   let symbols = await axios.get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)

   symbols = symbols.data


   var PATTERN = '/USDT'
   symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
   const chunkSize = 10
   let chunks = []
   for (let i = 0; i < 100; i += chunkSize) {
      chunks.push(symbols.slice(i, i + chunkSize))
   }

   // res.send({ status: 200, message: 'Berhasil Get Data Coin', data: req.body.interval })
   for await (const indicator of indicators) {
      for await (const chunk of chunks) {
         await update_coin(chunk, indicator, interval);
         await new Promise((resolve) => setTimeout(resolve, 200));
      }
   }
   await calculate_coin(interval)
   return false;
}

exports.manual_fetch = manual_fetch

// exports.calculate_now = async function (req, res) {
//    await calculate_coin('5m')
// }

// Function to check if symbol exists in array of objects
function symbolExists(dataArray, symbol, indicator = "") {
   for (let i = 0; i < dataArray.length; i++) {
      if (indicator != "") {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            return true;
         }
      } else {
         if (dataArray[i].symbol === symbol) {
            return true;
         }
      }
   }
   return false;
}

// Function to push data to existing symbol or create new object
function pushData(dataArray, symbol, data, indicator, date, status, max) {
   if (symbolExists(dataArray, symbol, indicator)) {
      for (let i = 0; i < dataArray.length; i++) {
         if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
            if (dataArray[i].date < date) {
               dataArray[i].date = date
            }
            if (dataArray[i].indicator === 'ma' && dataArray[i].date < date) {
               dataArray[i].last_price = data
            }
            if (dataArray[i].data.length < max) {
               dataArray[i].status.push(status)
               dataArray[i].data.push(data)
               break;
            }
         }
      }
   } else {
      dataArray.push({ symbol: symbol, data: [data], indicator: indicator, date: date, last_price: data, status: [status] });
   }
}

// cron.schedule('*/1 * * * *', async function () {
//    let res = await calculate_coin('5m')
//    console.log('Running cron calculate_coin(5m)');
// })
cron.schedule('1 * * * * *', async function () {
   console.log('Running cron calculate_coin(5m)');
   let res = await manual_fetch_function("5m")

})
// cron.schedule('0 */4 * * *', async function () {
//    let res = await calculate_coin('240m');
//    console.log('Running cron calculate_coin(240m)');
// })
// cron.schedule('* * * * *', async () => {
//    console.log('Running cron update_coin');
//    let symbols = await axios.get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)
//    symbols = symbols.data
//    var PATTERN = '/USDT'
//    symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
//    const chunkSize = 10
//    let chunks = []
//    for (let i = 0; i < symbols.length; i += chunkSize) {
//       chunks.push(symbols.slice(i, i + chunkSize))
//    }
//    for await (const indicator of indicators) {
//       for await (const chunk of chunks) {
//          await update_coin(chunk, indicator, '5m');
//          await new Promise((resolve) => setTimeout(resolve, 200));
//       }
//    }
// });

