//API Documentations Controller
//By Fikri akhdi
//Importing library
const ApiDocumentationsRepo = require('../repositories/api_documentations')
const apiHelper = require('../helpers/api')
//Function Log list
exports.list = async function (req, res) {
  res.type('json')
  try {
    var list = await ApiDocumentationsRepo.list()
    let formattedApiData = {}
    list.map((val)=>{
          let endpoint = val.api_url.split('/').slice(2).join('/')
          let type = val.is_production? 'production' : 'sandbox'
          if(!(endpoint in formattedApiData)) formattedApiData[endpoint] = {
            title:val.title,
            production:{},
            sandbox:{},
            api_type:val.api_type
          }
          formattedApiData[endpoint][type] = val
        })
    res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", Object.values(formattedApiData)))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}