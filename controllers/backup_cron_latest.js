//Cron Controller
//By Fikri akhdi
//Importing library
const coinsRepo = require('../repositories/coins')
const apiHelper = require('../helpers/api')
const axios = require('axios')
const moment = require('moment')
const jwt = require('jsonwebtoken')
var RateLimiter = require('limiter').RateLimiter;
const fs = require('fs')

//Function Update Coins
function wait(ms) {
  return new Promise( (resolve) => {setTimeout(resolve, ms)});
}
exports.update_coins = async function (req, res) {
  res.type('json')
  try {
    //get symbols
    const exchange = 'binance'
    const secret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbHVlIjoiNjNmNWRjYTJlYzkzMzI0NGEwZjI4MGQ5IiwiaWF0IjoxNjgwMDI0MjEwLCJleHAiOjMzMTg0NDg4MjEwfQ.eJACarJKO-R6PIxf0rrQ3rrNqylx5Tspy-Tkkk2T5J8'
    const interval = '5m'
    const indicators = [
      "ma",
      "ema",
      "dema",
      "kama",
      "macd",
      "mama",
      "hma",
    ]
    var indicator = req.params.indicator
    let symbols =  await axios
    .get(`https://api.taapi.io/exchange-symbols?secret=${secret}&exchange=binance`)
    symbols = symbols.data
    var PATTERN = '/USDT'
    symbols = symbols.filter(function (str) { return str.includes(PATTERN); })
    const datas = []
      symbols.map((e) => {
        datas.push({
          exchange,
          symbol: `${e}`,
          interval,
          indicators: [
            {
              id: e,
              indicator: indicator,
            },
          ],
        })
      })
    const chunkSize = 10
    let chunks = []
    const results = []
    for (let i = 0; i < datas.length; i += chunkSize) {
        chunks.push(datas.slice(i, i + chunkSize))
    }
    //check db
    //filter
    //lstone UFT/USDT
    const last = await coinsRepo.findLatestUpdate(indicator)
    var number_cut = 0
    var number_cut_child = 0
    if(last!=null){
      if(last.symbol=='OAX/USDT'){
        number_cut = 0
      } else {
        chunks.map((c, index)=>{
          c.map((d, index_child)=>{
            if(d.symbol == last.symbol){
              number_cut = index
              number_cut_child = index_child
            }
          })
        })
      }
    }
    var max_request = 75
    var jump = number_cut+max_request
    console.log(number_cut)
    console.log(jump)
    chunks = chunks.slice(number_cut,jump) //debug only
    var promises = []
    chunks.map(async o=>{
      promises.push(new Promise((resolve, reject) => {
          axios
          .post("https://api.taapi.io/bulk", {
            secret,
            construct: o,
          }).then(async bulk=>{
            results.push(bulk.data)
            resolve(bulk.data)
      })
    }))
    })

    Promise.all(promises).then( function (val) {
      //simpan ke db
      var promises_save = []
      val.map(o=>{
        console.log(JSON.stringify(o))
        o.data.map(p=>{
          promises_save.push(new Promise((resolve, reject) => {
          var dataCreate = {
          symbol: p.id,
          exchange,
          value:p.result.value,
          interval,
          indicator:req.params.indicator,
          status: null,
          date_created: Date.now(),
          }
         var coins =  coinsRepo.create(dataCreate)
         resolve(coins)
        }))
      })
      })

      Promise.all(promises_save).then( function (val) {
        res.type('json').send(apiHelper.api_format(true, "Update Coins Success"))
      })
    })
    // const max = 1
    // const results = []
    // const backtracked = []
    // for (let i = 0; i < max; i += 1) {
    //   var bulk = await axios
    //   .post("https://api.taapi.io/bulk", {
    //     secret,
    //     construct: chunks[i],
    //   })
    //   results.push(bulk.data)
    // }
    // results.map(async o=>{
    //   o.data.map(async p=>{await axios
    //     .post("https://api.taapi.io/bulk", {
    //       secret,
    //       construct: {
    //         exchange: "binance",
    //         symbol: `${p.id}`,
    //         interval: "5m",
    //         indicators: [
    //           {
    //             // MACD Backtracked 1
    //             id: `${p.id}`,
    //             indicator: "ma",
    //             backtracks: 10,
    //           },
    //         ],
    //       },
    //     }).then(resu=>{
    //       console.log(resu)
    //     })
    //   })
    // })
    // axios
    //     .post("https://api.taapi.io/bulk", {
    //       secret,
    //       construct: {
    //         exchange: "binance",
    //         symbol: `ONT/BTC`,
    //         interval: "5m",
    //         indicators: [
    //           {
    //             // MACD Backtracked 1
    //             id: `ONT/BTC`,
    //             indicator: "ma",
    //             backtracks: 10,
    //           },
    //         ],
    //       },
    //     }).then(result=>{
    //       console.log(JSON.stringify(result.data, null, 4))
    //     })
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

exports.dashboard = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
      var coins_raw = await coinsRepo.findLatestCoins(49280) //352*7*20
      var coins = []
      var promises = []
      var max = 20
      if(coins_raw.length>0){
        coins_raw.map(c=>{
              if(c.value!==null){
          pushData(coins,c.symbol, c.value, c.indicator, max)
        }
        })
      }
      coins.map(c=>{
        for(var i=1;i<=max;i++){
          if(c.data[i]>c.data[i-1]){
            c.nilai.push(1)
          } else { 
            c.nilai.push(0)
          }
        }
      })
      res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", coins))
      // })
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

// Function to check if symbol exists in array of objects
function symbolExists(dataArray, symbol, indicator) {
  for (let i = 0; i < dataArray.length; i++) {
    if (dataArray[i].symbol === symbol && dataArray[i].indicator  === indicator) {
      return true;
    }
  }
  return false;
}

// Function to push data to existing symbol or create new object
function pushData(dataArray, symbol, data, indicator, max) {
  if (symbolExists(dataArray, symbol, indicator)) {
    for (let i = 0; i < dataArray.length; i++) {
      if (dataArray[i].symbol === symbol && dataArray[i].indicator === indicator) {
          if(dataArray[i].data.length<max){
          dataArray[i].data.push(data);
          break;
          }
      }
    }
  } else {
    dataArray.push({ symbol: symbol, data: [data], nilai: [], indicator: indicator });
  }
}