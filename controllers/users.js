//Users Controller
//By Fikri akhdi
//Importing library
const axios = require('axios')
const coinsRepo = require('../repositories/coins')
const usersRepo = require('../repositories/users')
const companiesRepo = require('../repositories/companies')
const rolesRepo = require('../repositories/roles')
const bankAccountsRepo = require('../repositories/bank_accounts')
const whitelistsRepo = require('../repositories/whitelists')
const banksRepo = require('../repositories/banks')
const apiHelper = require('../helpers/api')
const permataHelper = require('../helpers/permata')
const mailerHelper = require('../helpers/mailer')
const useragent = require('express-useragent')
const randtoken = require('rand-token')
const crypto = require('crypto')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const {
  Validator
} = require('node-input-validator')

//Loading Key
var publicKEY = fs.readFileSync('./key/public.key', 'utf8')
var privateKEY = fs.readFileSync('./key/private.key', 'utf8')
var refreshTokens = []
//My Dashboard Function
exports.dashboard = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
    var where = {
      
    }
      var coins_raw = await coinsRepo.findByWhere(where)
      var coins = []
      if(coins_raw.length>0){
        coins_raw.map(c=>{
          if(coins[c.symbol]!==null && typeof coins[c.symbol]!== 'undefined'){
            coins[c.symbol].data.push(c.value)
          } else {
            coins[c.symbol] = {
              data: [c.value],
              status: 'red'
            }
          }
        })
      }
      console.log(coins)
      //count percentage 5/20
      //count status
      res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", coins))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}
//User List
exports.list = async function (req, res) {
  res.type('json')
  try {
    var user = await usersRepo.findById(res.user_id)
    if (user) {
    if(user.role.slug=='super-admin'){
        var user = await usersRepo.list()
        if (user) {
              res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", user))
        } else {
          res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
        }
    } else res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
  } else res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Company List
exports.company_list = async function (req, res) {
  res.type('json')
  try {
    var user = await usersRepo.findById(res.user_id)
    if (user) {
    if(user.role.slug=='super-admin'){
        var company = await companiesRepo.list()
        if (company) {
              res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", company))
        } else {
          res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
        }
    } else res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
  } else {
    res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
  }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//My Profile Function
exports.info = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
      console.log(res.user_id)
      var user = await usersRepo.findById(res.user_id)
      if (user) {
            res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", user))
      } else {
        res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
      }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//User Detail Function
exports.detail = async function (req, res) {
  res.type('json')
  try {
    if(user.role.slug=='super-admin'){
      var user = await usersRepo.findById(req.params.id)
      if (user) {
            res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", user))
      } else {
        res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
      }
    } else res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Delete User
exports.delete = async function (req, res) { 
  res.type('json')
  var dateNow = new Date()
  try {
    var user = await usersRepo.findById(res.user_id)
    if (user) {
      if(user.role.slug=='super-admin'){
        const validation = new Validator(req.body, {
          user_id: 'required',
        })
    var matched = await validation.check()
    if (matched) {
            var data_update = {
              is_deleted: true,
              date_modified: dateNow,
              modified_by: user.id
            }
            if(user.role.slug=='super-admin')
            await usersRepo.updateData(req.body.user_id, data_update)
          res.type('json').end(apiHelper.api_format(true, "Update Data Success"))
  } else {
    res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
  }
} else {
  res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
}
} else {
  res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
}
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Delete Company
exports.delete_company = async function (req, res) { 
  res.type('json')
  var dateNow = new Date()
  try {
    var user = await usersRepo.findById(res.user_id)
    if (user) {
      if(user.role.slug=='super-admin'){
        const validation = new Validator(req.body, {
          company_id: 'required',
        })
    var matched = await validation.check()
    if (matched) {
            var data_update = {
              is_deleted: true,
              date_modified: dateNow,
              modified_by: user.id
            }
            if(user.role.slug=='super-admin')
            await companiesRepo.updateData(req.body.company_id, data_update)
          res.type('json').end(apiHelper.api_format(true, "Update Data Success"))
  } else {
    res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
  }
} else {
  res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
}
} else {
  res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
}
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Update Profile
exports.update_profile = async function (req, res) { 
    res.type('json')
    var dateNow = new Date()
    try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
        var validatorOpt = {}
        if(user.role.slug=='super-admin'){
          validatorOpt = {
            user_id: 'required',
            name: 'required',
            phone_number: 'required',
            email: 'required|email',
            occupation: 'required',
            company_id: 'required'
          }
        } else {
          validatorOpt = {
            name: 'required',
            phone_number: 'required',
            email: 'required|email',
            occupation: 'required',
          }
        }
        const validation = new Validator(req.body, validatorOpt)
      var matched = await validation.check()
      if (matched) {
              var data_update = {
                name: req.body.name,
                phone_number: req.body.phone_number,
                email: req.body.email,
                occupation: req.body.occupation,
                date_modified: dateNow,
                modified_by: user.id
              }
              if(user.role.slug=='super-admin'){
                data_update.company_id = req.body.company_id
              await usersRepo.updateData(req.body.user_id, data_update)
              }
              else 
              await usersRepo.updateData(user.id, data_update)
            res.type('json').end(apiHelper.api_format(true, "Update Data Success"))
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } else {
    res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
  }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function Update Company
exports.update_company = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
        var whereValidation = {
          company_name: 'required',
          company_email: 'required|email',
          company_street: 'required',
          company_subdistrict: 'required',
          company_district: 'required',
          company_city: 'required',
          company_province: 'required',
          company_postal_code: 'required',
          company_country: 'required',
          company_domain: 'required',
          company_business_section: 'required'
        }
      if(user.role.slug=='super-admin'){
        whereValidation.company_id = 'required'
        whereValidation.company_transaction_fee = 'required'
    } 
    const validation = new Validator(req.body, whereValidation)
      var matched = await validation.check()
      if (matched) {
              var data_update = {
                name: req.body.company_name,
                email: req.body.company_email,
                street: req.body.company_street,
                subdistrict: req.body.company_subdistrict,
                district: req.body.company_district,
                city: req.body.company_city,
                province: req.body.company_province,
                postal_code: req.body.company_postal_code,
                country: req.body.company_country,
                domain: req.body.company_domain,
                business_section: req.body.company_business_section,
                date_modified: dateNow,
                modified_by: user.id
              }
              if(req.body.transaction_fee) data_update.transaction_fee = req.body.company_transaction_fee
              if(user.role.slug=='super-admin')
              await companiesRepo.updateData(req.body.company_id, data_update)
              else 
              await companiesRepo.updateData(user.company.id, data_update)
            res.type('json').end(apiHelper.api_format(true, "Update Data Success",))
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } else {
    res.type('json').end(apiHelper.api_format(false, 'Update Data Failed', 400))
  }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function Update API Token
exports.refresh_api_token = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
          var user = await usersRepo.findById(res.user_id)
          if (user) {
              var new_key = randtoken.uid(60)
              var new_password = randtoken.uid(15)
              var data_update = {
                api_token: new_key,
                api_password: new_password,
                date_modified: dateNow,
                updated_by: user.id
              }
              await usersRepo.updateData(user.id, data_update)
              var data_ret = {new_key, new_password}
            res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", data_ret))
          } else {
            res.type('json').end(apiHelper.api_format(false, 'Get Data Failed ', 400))
          }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function Bank Account
exports.bank_account = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
          var where = {
              company_id: user.company.id,
              is_deleted: null
          }
          var bank_accounts = await bankAccountsRepo.findByWhere(where)
        res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", bank_accounts))
      } else {
        res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
      }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Add Bank Account
exports.add_bank_account = async function (req, res) {
}

//Function Edit Bank Account
exports.edit_bank_account = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
        const validation = new Validator(req.body, {
            bank_id: 'required',
            bank_account_id: 'required',
            account_name: 'required',
            account_number: 'required',
        })
        //Checking Required Fields
        var matched = await validation.check()
        if (matched) {
                var user = await usersRepo.findById(res.user_id)
                if (user) {
                    var data_update = {
                        bank_id: req.body.bank_id,
                        account_name: req.body.account_name,
                        account_number: req.body.account_number,
                        date_updated: dateNow,
                        updated_by: user.id
                    }
                    await bankAccountsRepo.updateData(req.body.bank_account_id,data_update)
                    res.type('json').end(apiHelper.api_format(true, "Update Data Success", ""))
                } else {
                    res.type('json').end(apiHelper.api_format(false, 'Update Data Failed', 400))
                }
            } else {
                res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
            }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function Delete Bank Account
exports.delete_bank_account = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
        const validation = new Validator(req.body, {
            bank_account_id: 'required',
        })
        //Checking Required Fields
        var matched = await validation.check()
        if (matched) {
                var user = await usersRepo.findById(res.user_id)
                if (user) {
                    var data_update = {
                        is_deleted: true,
                        date_updated: dateNow,
                        updated_by: user.id
                    }
                    await bankAccountsRepo.updateData(req.body.bank_account_id,data_update)
                    res.type('json').end(apiHelper.api_format(true, "Delete Data Success"))
                } else {
                    res.type('json').end(apiHelper.api_format(false, 'Delete Data Failed', 400))
                }
            } else {
                res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
            }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function create user
exports.create = async function (req, res) {
  res.type('json')
  //Fetching Data
    try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
      if(user.role.slug=='super-admin' || user.role.slug=='admin'){
      const validation = new Validator(req.body, {
        name: 'required',
        phone_number: 'required',
        email: 'required|email',
        password: 'required',
        occupation: 'required',
        role_id: 'required'
      })
    
      //Checking Required Fields
      var matched = await validation.check()
      if (matched) {
        var email = req.body.email.toLowerCase().replace(/\s/g, '')
          var where = {
            email : email,
            is_deleted: null,
          }
          var userCheck = await usersRepo.findByWhere(where)
          if(userCheck.length==0){
            var user_create = {
              name: req.body.name,
              phone_number: req.body.phone_number,
              company_id: req.body.company_id,
              email: email,
              password: crypto.createHash('sha256').update(req.body.password).digest('base64'),
              occupation: req.body.occupation,
              role_id: req.body.role_id,
              created_by: res.user_id,
              date_created: Date.now(),
              api_token: randtoken.uid(60).toString(),
              api_password: randtoken.uid(15).toString(),
              is_active: false
            }
            var user = await usersRepo.create(user_create)
            var jobData = {
                confirm_link: process.env.BASE_URL + "/verify/" + user.id,
                email_to: req.body.email,
                year: new Date().getFullYear(),
            }
            const job = reqisterQueue.createJob(jobData);
            job.save();
            job.on('succeeded', (result) => {
              console.log(`Received`);
              console.log(jobData)
            });
            res.type('json').end(apiHelper.api_format(true, "Register success", null, user))
            //sending email for verification
            res.type('json').end(apiHelper.api_format(true, "Authorization success", "", data_ret))
          } else {
            res.type('json').end(apiHelper.api_format(false, "Email is already registered", 400))
          }
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } else {
    res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
  }
} else {
  res.type('json').end(apiHelper.api_format(false, 'User not Found', 400))
}
    } catch (e) {
      console.log(e)
      res.type('json').status(401)
      res.type('json').end(apiHelper.api_format(false, "Something went wrong", 401, e))
    }
}

//Function create company
exports.create_company = async function (req, res) {
  res.type('json')
  //Fetching Data
    try {
      var user = await usersRepo.findById(res.user_id)
      if (user) {
      if(user.role.slug=='super-admin'){
      const validation = new Validator(req.body, {
        company_email: 'required|email',
        company_name: 'required',
        company_street: 'required',
        company_city: 'required',
        company_state: 'required',
        company_postal_code: 'required',
        company_country: 'required',
        company_domain: 'required',
        company_business_section: 'required',
        company_transaction_fee: 'required'
      })
    
      //Checking Required Fields
      var matched = await validation.check()
      if (matched) {
      var company_create = {
        name: req.body.company_name,
        email: req.body.company_email,
        street: req.body.company_street,
        city: req.body.company_city,
        state: req.body.company_state,
        postal_code: req.body.company_postal_code,
        country: req.body.company_country,
        domain: req.body.company_domain,
        business_section: req.body.company_business_section,
        transaction_fee: req.body.company_transaction_fee,
        date_created: Date.now()
      }
      var where = {
        email : req.body.company_email,
        is_deleted: null
      }
      var companyCheck = await companiesRepo.findByWhere(where)
      if(companyCheck.length==0){
        await companiesRepo.create(company_create)
        res.type('json').end(apiHelper.api_format(true, "Create Company Success"))
        } else {
          res.type('json').end(apiHelper.api_format(false, "Company is already registered", 400))
        }
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } else {
    res.type('json').end(apiHelper.api_format(false, 'You have no right to access this action', 400))
  }
} else {
  res.type('json').end(apiHelper.api_format(false, 'User not Found', 400))
}
    } catch (e) {
      console.log(e)
      res.type('json').status(401)
      res.type('json').end(apiHelper.api_format(false, "Something went wrong", 401, e))
    }
}


//Function register
exports.register = async function (req, res) {
    res.type('json')
    //Fetching Data
      try {
        const validation = new Validator(req.body, {
          company_email: 'required|email',
          company_name: 'required',
          company_street: 'required',
          company_subdistrict: 'required',
          company_district: 'required',
          company_city: 'required',
          company_province: 'required',
          company_postal_code: 'required',
          company_country: 'required',
          company_domain: 'required',
          company_business_section: 'required',
          name: 'required',
          phone_number: 'required',
          email: 'required|email',
          password: 'required',
          occupation: 'required'
        })
      
        //Checking Required Fields
        var matched = await validation.check()
        if (matched) {
        var company_create = {
          name: req.body.company_name,
          email: req.body.company_email,
          street: req.body.company_street,
          subdistrict: req.body.company_subdistrict,
          district: req.body.company_district,
          city: req.body.company_city,
          province: req.body.company_province,
          postal_code: req.body.company_postal_code,
          country: req.body.company_country,
          domain: req.body.company_domain,
          business_section: req.body.company_business_section,
          date_created: Date.now()
        }
        var where = {
          email : req.body.company_email
        }
        var companyCheck = await companiesRepo.findByWhere(where)
        if(companyCheck.length==0){
          var companyId = await companiesRepo.create(company_create)
          if(companyId){
            var where = {
              email : req.body.email,
              is_deleted: null,
            }
            var userCheck = await usersRepo.findByWhere(where)
            if(userCheck.length==0){
              var where = {
                slug:"admin"
              }
              var role = await rolesRepo.findByWhere(where)
              var user_create = {
                name: req.body.name,
                phone_number: req.body.phone_number,
                email: req.body.email,
                password: req.body.password,
                occupation: req.body.occupation,
                company_id: companyId.id,
                role_id: role[0].id,
                date_created: Date.now(),
                api_token: randtoken.uid(60).toString(),
                api_password: randtoken.uid(15).toString(),
                is_active: false
              }
              var user = await usersRepo.create(user_create)
              var code = `${user.id.toString()}:${user.email.toString()}`
              var encoded_id = Buffer.from(code).toString('base64')
              var jobData = {
                  confirm_link: process.env.BASE_URL_DASHBOARD + "/verify/",
                  code: encoded_id,
                  email_to: req.body.email,
                  year: new Date().getFullYear(),
              }
              const job = reqisterQueue.createJob(jobData);
              job.save();
              job.on('succeeded', (result) => {
                console.log(`Received`);
                console.log(jobData)
              });
              res.type('json').end(apiHelper.api_format(true, "Register success", null, user))
            } else {
              res.type('json').end(apiHelper.api_format(false, "Email is already registered", 400))
            }
          } else {
            res.type('json').end(apiHelper.api_format(false, "Something went wrong", 400))
          }
        } else {
          res.type('json').end(apiHelper.api_format(false, "The Company is already registered", 400))
        }
      } else {
        res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
      }
      } catch (e) {
        console.log(e)
        res.type('json').end(apiHelper.api_format(false, "Something went wrong", 401, e))
      }
}

//Function Change Password
exports.change_password = async function (req, res) {
  res.type('json')
  var dateNow = new Date()
  try {
    const validation = new Validator(req.body, {
      old_password: 'required',
      new_password: 'required',
    })
    //Checking Required Fields
    var matched = await validation.check()
    if (matched) {
      var where = {
        password : crypto.createHash('sha256').update(req.body.old_password).digest('base64')
      }
        var user = await usersRepo.findByWhere(where)
        if (user.length>0) {
            var data_update = {
              password: crypto.createHash('sha256').update(req.body.new_password).digest('base64'),
              date_modified: dateNow,
              updated_by: user[0].id
            }
            await usersRepo.updateData(user[0].id, data_update)
          res.type('json').end(apiHelper.api_format(true, "Change Password Success"))
        } else {
          res.type('json').end(apiHelper.api_format(false, 'Current Password does not matched', 400))
        }
  } else {
    res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
  }
  } catch (e) {
    console.error(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

exports.dashboard = async function (req, res) {
}

//Function White List
exports.whitelists = async function (req, res) {
  res.type('json')
  var user = await usersRepo.findById(res.user_id)
  try{
  if (user) {
      var where = {
          company_id: user.company.id,
          is_deleted: null
      }
      var whitelists = await whitelistsRepo.findByWhere(where)
    res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", whitelists))
  } else {
    res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
  }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Add White List
exports.add_whitelists = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
      const validation = new Validator(req.body, {
          ip_address: 'required'
      })
      var matched = await validation.check()
      if (matched) {
          var user = await usersRepo.findById(res.user_id)
          if (user) {
              var data_create = {
                  ip_address: req.body.ip_address,
                  company_id: user.company.id,
                  date_created: dateNow,
                  created_by: user.id
              }
              var whitelists = await whitelistsRepo.create(data_create)
            res.type('json').end(apiHelper.api_format(true, "Get Data Success", "", whitelists))
          } else {
            res.type('json').end(apiHelper.api_format(false, 'Get Data Failed', 400))
          }
    } else {
      res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}

//Function Delete White List
exports.delete_whitelists = async function (req, res) {
    res.type('json')
    var dateNow = new Date()
    try {
        const validation = new Validator(req.body, {
            whitelist_id: 'required',
        })
        //Checking Required Fields
        var matched = await validation.check()
        if (matched) {
                var user = await usersRepo.findById(res.user_id)
                if (user) {
                    var data_update = {
                        is_deleted: true,
                        date_updated: dateNow,
                        updated_by: user.id
                    }
                    await whitelistsRepo.updateData(req.body.whitelist_id,data_update)
                    res.type('json').end(apiHelper.api_format(true, "Delete Data Success"))
                } else {
                    res.type('json').end(apiHelper.api_format(false, 'Delete Data Failed', 400))
                }
            } else {
                res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
            }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
}