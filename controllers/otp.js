//OTP Controller
//By Fikri akhdi
//Importing library
const usersRepo = require('../repositories/users')
const banksRepo = require('../repositories/banks')
const otpRepo = require('../repositories/otp')
const apiHelper = require('../helpers/api')
const randtoken = require('rand-token');
const crypto = require('crypto')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const fs = require('fs')

const {
  Validator
} = require('node-input-validator')
//Function Request OTP
exports.request = async function (req, res) {
  res.type('json')
  try {
    const validation = new Validator(req.body, {
        type: 'required'
    })
    var matched = await validation.check()
    if (matched) {
        if (res.user_id!=null) {
            var user = await usersRepo.findById(res.user_id)
            if (user) {
                var dataCreate = {
                    user_id : user.id,
                    code: randtoken.uid(5),
                    type: req.body.type, //edit profile, edit company, withdraw, topup
                    date_created: Date.now(),
                    is_used: false,
                    created_by: user.id
                }
                await otpRepo.create(dataCreate)
                //send email and sms
                //send mail
                  var jobData = {
                    code: dataCreate.code,
                    email_to: user.email,
                    year: new Date().getFullYear(),
                }

                const job = otpQueue.createJob(jobData);
                job.save();
                job.on('succeeded', (result) => {
                  console.log(`Received`);
                  console.log(jobData)
                });
                res.type('json').end(apiHelper.api_format(true, "Request OTP Success"))
            } else {
                res.type('json').end(apiHelper.api_format(false, "Invalid User",400))
            }
        }
    } else {
        res.type('json').send(apiHelper.api_format(false, "Input fields error", 400, validation.errors))
    }
  } catch (e) {
    console.log(e)
    res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
  }
}

//Function Check OTP
exports.check = async function (req, res) {
    res.type('json')
    try {
        var where = {
            code: req.params.code,
            is_used: false
        }
      var otp = await otpRepo.findByWhere(where)
      if(otp.length>0){
        var dataUpdate = {
            is_used:true,
            date_updated: Date.now(),
            updated_by: res.user_id
        }
        await otpRepo.update(dataUpdate,{
          where: {
            code: req.params.code,
            user_id:res.user_id
          }})
        res.type('json').end(apiHelper.api_format(true, "Check OTP Success"))
      } else {
        res.type('json').end(apiHelper.api_format(false, "Invalid OTP", 400))
      }
    } catch (e) {
      console.log(e)
      res.type('json').send(apiHelper.api_format(false, "Something went wrong", 400))
    }
  }

