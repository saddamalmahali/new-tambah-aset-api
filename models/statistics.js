const mongoose = require("mongoose")

const { Schema } = mongoose

const statisticSchema = new Schema({
    symbol: {
        type: Schema.Types.String,
        required: true

    },
    interval_minute: {
        type: Schema.Types.String,
        required: true
    },
    percentage: {
        type: Schema.Types.Decimal128,
        required: true,
        default: 0,
        get: getCosts

    },
    last_price: {
        type: Schema.Types.String,
        required: true
    },
    date: {
        type: Schema.Types.Date
    },
})

function getCosts(value) {
    if (typeof value !== 'undefined') {
        return parseFloat(value.toString());
    }
    return value;
};
module.exports = mongoose.model("statistics", statisticSchema)