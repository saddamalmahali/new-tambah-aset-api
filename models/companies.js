// //By Fikri akhdi
// module.exports = (Sequelize, DataTypes) => {
//     return Sequelize.define('companies', {
//         id: {
//             type: DataTypes.INTEGER,
//             primaryKey: true,
//             autoIncrement: true
//         },
//         name: DataTypes.CHAR,
//         email:  DataTypes.CHAR(100),
//         street: DataTypes.CHAR(100),
//         subdistrict: DataTypes.INTEGER,
//         district: DataTypes.INTEGER,
//         city: DataTypes.INTEGER,
//         province: DataTypes.INTEGER,
//         postal_code: DataTypes.CHAR(100),
//         is_deleted: DataTypes.BOOLEAN,
//         country: DataTypes.CHAR(100),
//         domain:  DataTypes.CHAR(100),
//         business_section: DataTypes.INTEGER,
//         transaction_fee: DataTypes.DECIMAL,
//         date_created: DataTypes.DATE,
//         date_modified: DataTypes.DATE
//     })
// }