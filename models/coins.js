//By Fikri akhdi
// module.exports = (Sequelize, DataTypes) => {
//     return Sequelize.define('coins', {
//       id: {
//         type: DataTypes.INTEGER,
//         primaryKey: true,
//         autoIncrement: true
//       },
//       symbol: DataTypes.CHAR(100),
//       exchange: DataTypes.CHAR(100),
//       value: DataTypes.STRING,
//       interval:  DataTypes.CHAR(100),
//       indicator: DataTypes.STRING,
//       status: DataTypes.CHAR(20),
//       date_created: DataTypes.DATE,
//     })
//   }
const mongoose = require('mongoose')

const { Schema } = mongoose

// Schema
// ---------------------------------------
const coinSchema = new Schema({
    symbol: {
        type: Schema.Types.String,
        required: true

    },
    exchange: {
        type: Schema.Types.String,
        required: true
    },
    value: {
        type: Schema.Types.String,
        required: true
    },
    interval: {
        type: Schema.Types.String,
        required: true
    },
    indicator: {
        type: Schema.Types.String,
        required: true
    },
    status: {
        type: Schema.Types.String
    },
    date_created: {
        type: Schema.Types.String
    },
})

module.exports = mongoose.model("coins", coinSchema)